/* Includes ------------------------------------------------------------------*/
#include <main.h>

/* Private define-------------------------------------------------------------*/

/* Private variables----------------------------------------------------------*/
static void GPIO_Init(void);           //通用输入输出端口初始化
static void IE_init(void);             //中断初始化
static void Power_ON_Indication(void); //上电指示
static void Sys_Init(void);            //系统初始化

/* Public variables-----------------------------------------------------------*/
Hardware_Init_t Hardware_Init = 
{
	GPIO_Init,
	IE_init,
	Sys_Init
};

/* Private function prototypes------------------------------------------------*/      

/*
	* @name   GPIO_Init
	* @brief  通用输入输出端口初始化
	* @param  None
	* @retval None      
*/
static void GPIO_Init()
{

	
	//M1  M0
		//0   0	     准双向口
		//0   1	     推挽输出
		//1   0	     高阻输入
		//1   1	     开漏

	//对JQD的GPIO进行配置 设置为准双向口
/*
#define 	JDQ1 	P07 //上
#define		JDQ2 	P06	//下

//220V
#define 	JDQ8 	P47	//加热
#define 	JDQ9 	P12	//风扇
#define 	JDQ10 	P13	//风机
#define 	JDQ11 	P14	//点火
#define 	JDQ12 	P15	//点火棒
#define 	JDQ13 	P16	//点火棒
*/
	P0M0 = 0xC0;//1100 0000
	P0M1 = 0x00;//0000 0000
	


	
	/*

	
	#串口二的设计 P10 P11
	

	*/
	P1M0 = 0x7E;//0111 1110
	P1M1 = 0x01;//0000 0001
	

	/*
		对MAX6675GPIO设置为
	
	#define SCK  P27	推挽输出
	#define CS 	P26		推挽输出
	#define MISO P25	浮空输入
	*/
	P2M0 = 0xC0;//1100 0000
	P2M1 = 0x3F;//0011 1111
	


	

	
	P4M0 = 0x00;// 0000 0000
	P4M1 = 0x0C;// 0001 1100

	/*
	对于遥控器的GPIO进行设置高阻输入
	#define Q1	P24
	#define Q2	P23
	#define Q3	P22
	#define Q4	P21
	#define Q5	P20
	#define Q6	P44
	#define Q7	P43
	#define Q8	P42
	*/

	P5M1 = 0x30;
	P5M0 = 0x00;


	/*
		对于串口进行GPIO引脚配置
		P30		高阻输入
		P31		推挽输出
	*/
	P3M0 = 0x02;
	P3M1 = 0x01;
	
	P26 = 1;
	P27 = 1;

	JDQ1 = CLOSE;
	JDQ2 = CLOSE;
	JDQ8 = CLOSE;
	JDQ9 = CLOSE;
	JDQ10 = CLOSE;
	JDQ11 = CLOSE;
	JDQ12 = CLOSE;
	JDQ13 = CLOSE;

	
	LED1_HOT = OFF;
	LED2_FAN = OFF;
	LED3_MTR = OFF;
	LED4_FIR = OFF;
	LED5_RUN = OFF;
	LED6_SBY = OFF;
	
	
	

}

/*
	* @name   IE_init
	* @brief  中断初始化
	* @param  None
	* @retval None      
*/
static void IE_init()
{	


	ET0 =  1;            //打开定时器0中断
	
	ES  = 1;              //使能串口1中断
	IE2 |= BIT0;          //使能串口2中断
	
	EA  =  1;       			//打开总中断
	
	
	
}



/*
	* @name   Sys_Init
	* @brief  系统初始化
	* @param  None
	* @retval None      
*/
static void Sys_Init()
{	
	
	Public.Delay_ms(3000);                //上电延时5s
	Hardware_Init.GPIO_Init();          //GPIO口初始化
	Timer0.Timer0_Init();               //定时器0初始化
	Hardware_Init.IE_init();            //中断初始化
	UART1.UART_Init();                  //串口1初始化
	UART2.UART_Init();                  //串口2初始化


}

/********************************************************
  End Of File
********************************************************/