#ifndef __MAX6675_H_
#define __MAX6675_H_


#define SCK  P27
#define CS 	P26
#define MISO P25



typedef struct{

	uint16_t temperature;
	uint16_t hightemp;
	uint16_t lowtemp;

	
	uint8_t (*max6675_readWriteByte)(uint8_t txData);
	uint16_t (*max6675_readRawValue)(void);
	uint16_t (*max6675_readTemperature) (void);

} Temperature_t;



/* extern variables-----------------------------------------------------------*/
extern Temperature_t temperature_t;
/* extern function prototypes-------------------------------------------------*/

#endif
/********************************************************
  End Of File
********************************************************/


