#ifndef __PUBLIC_H_
#define __PUBLIC_H_

//数据类型重定义
typedef	signed   char      sint8_t;
typedef signed   short int sint16_t;
typedef signed   long  int sint32_t;

typedef unsigned char      uint8_t;
typedef unsigned short int uint16_t;
typedef unsigned long  int uint32_t;

//定义枚举类型 -> BIT位
typedef enum
{
	BIT0 = (uint8_t)(0x01 << 0),
	BIT1 = (uint8_t)(0x01 << 1),
	BIT2 = (uint8_t)(0x01 << 2),
	BIT3 = (uint8_t)(0x01 << 3),
	BIT4 = (uint8_t)(0x01 << 4),
	BIT5 = (uint8_t)(0x01 << 5),
	BIT6 = (uint8_t)(0x01 << 6),
	BIT7 = (uint8_t)(0x01 << 7),
}BIT_t;



//定义枚举类型 -> TRUE/FALSE位
typedef enum
{
	TRUE  = (bit)1,
	FALSE = (bit)0,
}BOOL_t;

//定义结构体类型
typedef struct
{
	void (*Delay_ms)(uint16_t);            //ms延时函数
	void (*SPI_delay)(void);
	void (*Memory_Clr)(uint8_t* pucBuffer,uint16_t LEN);
} Public_t;

/* extern variables-----------------------------------------------------------*/
extern Public_t  Public;


#define Monitor_Run_Code      1 //代码运行监控器 
#endif
/********************************************************
  End Of File
********************************************************/