

/******************************************************************************
  * @file	 main.c 
  * @author  公烁
  * @version V1.1
  * @date	 2022-7-23
  * @Conpany 
  * @project 艾灸
*******************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include <main.h>

/* Private define-------------------------------------------------------------*/

/* Private variables----------------------------------------------------------*/
uint8_t xdata	dataUpdate_Buffer[20] =
{
	0, 
};

uint8_t 		StateFlag = 0;
uint8_t 		StandBy_Flag = 0;



/* Public variables-----------------------------------------------------------*/

/* Private function prototypes------------------------------------------------*/

/*
	* @name   main
	* @brief  主函数
	* @param  None	
	* @retval None		
*/
void main()
{


	Hardware_Init.Sys_Init();						//系统初始化


	while (1)
	{
		if (System.ucSystem_Status == System_RUN)
		{
			System.Run();
		}
		else
		{
			System.StandBy();
		}
	}
}


// 1:	串口1 单独发
// 2：串口2 单独发
// 3：串口1与2 都发
void DataUpdate(uint8_t flag)
{
	uint8_t 		Flag = flag;

	//地址码
	dataUpdate_Buffer[0] = Modbus.Head1;
	dataUpdate_Buffer[1] = Modbus.Head2;

	//功能码
	dataUpdate_Buffer[2] = 0x03;

	//数据长度
	dataUpdate_Buffer[3] = 15;

	//温度
	dataUpdate_Buffer[4] = 0;
	dataUpdate_Buffer[5] = temperature_t.temperature;

	//继电器
	dataUpdate_Buffer[6] = 0;
	dataUpdate_Buffer[7] = JDQ_Run.JDQ_1_FLAG;
	dataUpdate_Buffer[8] = JDQ_Run.JDQ_2_FLAG;
	dataUpdate_Buffer[9] = JDQ_Run.JDQ_3_FLAG;
	dataUpdate_Buffer[10] = JDQ_Run.JDQ_4_FLAG;
	dataUpdate_Buffer[11] = JDQ_Run.JDQ_5_FLAG;
	dataUpdate_Buffer[12] = JDQ_Run.JDQ_6_FLAG;

	dataUpdate_Buffer[13] = JDQ_Run.JDQ_8_FLAG;
	dataUpdate_Buffer[14] = JDQ_Run.JDQ_9_FLAG;
	dataUpdate_Buffer[15] = JDQ_Run.JDQ_10_FLAG;
	dataUpdate_Buffer[16] = JDQ_Run.JDQ_11_FLAG;
	dataUpdate_Buffer[17] = JDQ_Run.JDQ_11_FLAG;
	dataUpdate_Buffer[18] = JDQ_Run.JDQ_11_FLAG;

	//将更新的数据发送给
	switch (flag)
		{
		case 1:
			UART1.UART_SendArray(dataUpdate_Buffer, 19);
			break;

		case 2:
			UART2.UART_SendArray(dataUpdate_Buffer, 19);
			break;

		case 3:
			UART2.UART_SendArray(dataUpdate_Buffer, 19);
			Public.Delay_ms(50);
			UART1.UART_SendArray(dataUpdate_Buffer, 19);
			break;

		default:
			break;

	}
		Public.Memory_Clr(dataUpdate_Buffer,20);//清除数据


}






/********************************************************
  End Of File
********************************************************/
