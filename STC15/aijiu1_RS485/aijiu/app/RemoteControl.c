/* Includes ------------------------------------------------------------------*/
#include <main.h>

/* Private define-------------------------------------------------------------*/
#define DIANDONG 			1
/* Private variables----------------------------------------------------------*/


/* Private function prototypes------------------------------------------------*/    

static void QKEY_Detect(void);   //按钮开关检测
/* Public variables-----------------------------------------------------------*/
//结构体初始化
RemoteControl_t RemoteControl_1 ={FALSE,QKEY_Detect};


/*
	* @name   KEY_Detect
	* @brief  按钮开关检测
	* @param  None
	* @retval None      
*/

static void QKEY_Detect(){
	//单击检测
	if (Q1||Q2||Q3||Q4||Q5||Q6||Q7||Q8)
	{
		//消抖
		Public.Delay_ms(200);
		if(Q1)
		{
			RemoteControl_1.QKEY_Clicked = TRUE;
			//UART1.UART_SendString("Q1 is click");
			#if  DIANDONG
			JDQ_Run.JDQ_1_FLAG = 1;
			JDQ1 = 0;
			JDQ2 = 1;
			while(Q1)
			{
				Public.Delay_ms(10);
			}
			JDQ1 = 1;
			JDQ2 = 1;
      JDQ_Run.JDQ_1_FLAG = 0;
			#else
			
      #endif
			
			Trigger_1.trigger_state = 1;
		}
		if(Q2)
		{
			RemoteControl_1.QKEY_Clicked = TRUE;
			//UART1.UART_SendString("Q2 is click");
			#if  DIANDONG
			JDQ_Run.JDQ_2_FLAG = 1;
			JDQ1 = 1;
			JDQ2 = 0;
			while(Q2)
			{
				Public.Delay_ms(10);
			}
			JDQ1 = 1;
			JDQ2 = 1;
      JDQ_Run.JDQ_2_FLAG = 0;
			#else
					JDQ_Run.JDQ_2_FLAG = !JDQ_Run.JDQ_2_FLAG;
      #endif
			Trigger_1.trigger_state = 2;
		}
		if(Q3)
		{
			RemoteControl_1.QKEY_Clicked = TRUE;
			//UART1.UART_SendString("Q3 is click");
			
			#if  DIANDONG
			JDQ_Run.JDQ_3_FLAG = 1;
			JDQ3 = 0;
			JDQ4 = 1;
			StrokeSwitch1.Key1_Flag = FALSE;
			while(Q3)
			{
				Public.Delay_ms(10);
			}
			JDQ3 = 1;
			JDQ4 = 1;
      JDQ_Run.JDQ_3_FLAG = 0;
			#else
					JDQ_Run.JDQ_3_FLAG = !JDQ_Run.JDQ_3_FLAG;
      #endif
			Trigger_1.trigger_state = 3;
		}
		
		if(Q4)
		{
			RemoteControl_1.QKEY_Clicked = TRUE;
			//UART1.UART_SendString("Q4 is click");
			#if  DIANDONG
			JDQ_Run.JDQ_4_FLAG = 1;
			if(StrokeSwitch1.Key1_Flag==FALSE){
			JDQ3 = 1;
			JDQ4 = 0;
			}
			while(Q4)
			{
				Public.Delay_ms(10);
			}
			JDQ3 = 1;
			JDQ4 = 1;
      JDQ_Run.JDQ_4_FLAG = 0;
			#else
					JDQ_Run.JDQ_4_FLAG = !JDQ_Run.JDQ_4_FLAG;
      #endif
			Trigger_1.trigger_state = 4;
		}
		if(Q5)
		{
			RemoteControl_1.QKEY_Clicked = TRUE;
			//UART1.UART_SendString("Q5 is click");
			#if  DIANDONG
			JDQ_Run.JDQ_5_FLAG = 1;
			if(StrokeSwitch1.Key2_Flag==FALSE){
			JDQ5 = 0;
			JDQ6 = 1;
			}
			while(Q5)
			{
				Public.Delay_ms(10);
			}
			JDQ5 = 1;
			JDQ6 = 1;
      JDQ_Run.JDQ_5_FLAG = 0;
			#else
					JDQ_Run.JDQ_5_FLAG = !JDQ_Run.JDQ_5_FLAG;
      #endif
			Trigger_1.trigger_state = 5;
		}
		if(Q6)
		{
			RemoteControl_1.QKEY_Clicked = TRUE;
			//UART1.UART_SendString("Q6 is click");
			#if  DIANDONG
			JDQ_Run.JDQ_6_FLAG = 1;
			JDQ5 = 1;
			JDQ6 = 0;
			StrokeSwitch1.Key2_Flag = FALSE;
			while(Q6)
			{
				Public.Delay_ms(10);
			}
			JDQ5 = 1;
			JDQ6 = 1;
      JDQ_Run.JDQ_6_FLAG = 0;
			#else
					JDQ_Run.JDQ_6_FLAG = !JDQ_Run.JDQ_6_FLAG;
      #endif
			Trigger_1.trigger_state = 6;
		}
		
		if(Q7)
		{	
			RemoteControl_1.QKEY_Clicked = TRUE;
			JDQ_Run.JDQ_11_FLAG = !JDQ_Run.JDQ_11_FLAG;
			if(JDQ_Run.JDQ_11_FLAG==1)
			{
				Trigger_1.trigger_state = 11;
			}
			else
			{
				Trigger_1.trigger_state = 12;
			}
		}
		if(Q8)
		{
			RemoteControl_1.QKEY_Clicked = TRUE;
			JDQ_Run.JDQ_8_FLAG = !JDQ_Run.JDQ_8_FLAG;
			if(JDQ_Run.JDQ_8_FLAG)
			{
					JDQ_Run.JDQ_9_FLAG = 1;
					JDQ_Run.JDQ_10_FLAG = 1;
			}
			Trigger_1.trigger_state = 14;
			
		}
	}
	
} 

/********************************************************
  End Of File
********************************************************/
