/* Includes ------------------------------------------------------------------*/
#include <main.h>

/* Private define-------------------------------------------------------------*/
#define  KEY1_Status 	P32
#define  KEY2_Status 	P33
#define  KEY3_Status 	P36

/* Private variables----------------------------------------------------------*/

//结构体定义
StrokeSwitch_t StrokeSwitch1 = {FALSE,FALSE,FALSE};





/* Private function prototypes------------------------------------------------*/      






void INT0_isr() interrupt  0
{
	if(P32==1){
		JDQ4 = 1;
		JDQ3 = 1;
	StrokeSwitch1.Key1_Flag = TRUE;
	}
	else
	{
		StrokeSwitch1.Key1_Flag = FALSE;
	}

}

void INT1_isr() interrupt  2
{
	if(P33==1){
		JDQ5 = 1;
		JDQ6 = 1;
	StrokeSwitch1.Key2_Flag = TRUE;
	}
	else
	{
		StrokeSwitch1.Key2_Flag = FALSE;
	}
}

void INT2_isr() interrupt  10
{

	StrokeSwitch1.Key3_Flag = TRUE;

}
/********************************************************
  End Of File
********************************************************/