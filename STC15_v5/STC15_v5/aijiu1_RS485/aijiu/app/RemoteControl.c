

/* Includes ------------------------------------------------------------------*/
#include <main.h>

/* Private define-------------------------------------------------------------*/
#define DIANDONG 1

/* Private variables----------------------------------------------------------*/
/* Private function prototypes------------------------------------------------*/
static void QKEY_Detect(void); //按钮开关检测
static void	QKEY_StandBy_Flag_Detect(void);// 模式切换按键检测
/* Public variables-----------------------------------------------------------*/
//结构体初始化
RemoteControl_t RemoteControl_1 =
	{
		FALSE, QKEY_Detect,QKEY_StandBy_Flag_Detect};

/*
 * @name   KEY_Detect
 * @brief  按钮开关检测
 * @param  None
 * @retval None
 */
static void QKEY_Detect()
{
	//单击检测
	if (Q1 || Q2 || Q3 || Q4 )
//	if (Q1 || Q2 || Q3 || Q4 || Q5 || Q6 || Q7 || Q8)
	{
		//消抖
		Public.Delay_ms(200);

		if (Q1)
		{
			RemoteControl_1.QKEY_Clicked = TRUE;

			// UART1.UART_SendString("Q1 is click");
#if DIANDONG
			JDQ_Run.JDQ_1_FLAG = 1;
			JDQ1 = OPEN;
			JDQ2 = CLOSE;

			while (Q1)
			{
				Public.Delay_ms(10);
			}

			JDQ1 = CLOSE;
			JDQ2 = CLOSE;
			JDQ_Run.JDQ_1_FLAG = 0;

#else

#endif

			Trigger_1.trigger_state = 1;
		}

		if (Q2)
		{
			RemoteControl_1.QKEY_Clicked = TRUE;

			// UART1.UART_SendString("Q2 is click");
#if DIANDONG
			JDQ_Run.JDQ_2_FLAG = 1;
			JDQ1 = CLOSE;
			JDQ2 = OPEN;

			while (Q2)
			{
				Public.Delay_ms(10);
			}

			JDQ1 = CLOSE;
			JDQ2 = CLOSE;
			JDQ_Run.JDQ_2_FLAG = 0;

#else

			JDQ_Run.JDQ_2_FLAG = !JDQ_Run.JDQ_2_FLAG;
#endif

			Trigger_1.trigger_state = 2;
		}

		if (Q3)
		{

			//一键启动
			RemoteControl_1.QKEY_Clicked = TRUE;
			JDQ_Run.JDQ_8_FLAG = 1;
			Trigger_1.trigger_state = 14;


		}
		
		if (Q4)
		{
			
			//一键关闭
			RemoteControl_1.QKEY_Clicked = TRUE;
			JDQ_Run.JDQ_8_FLAG = 0;
			Trigger_1.trigger_state = 14;
		}

	}
}



static void	QKEY_StandBy_Flag_Detect(void)
{
	
	if (Q1||Q2||Q4)
		{
			//消抖
			Public.Delay_ms(200);
			if (Q1)
		{
			RemoteControl_1.QKEY_Clicked = TRUE;

			// UART1.UART_SendString("Q1 is click");
#if DIANDONG
			JDQ_Run.JDQ_1_FLAG = 1;
			JDQ1 = OPEN;
			JDQ2 = CLOSE;

			while (Q1)
			{
				Public.Delay_ms(10);
			}

			JDQ1 = CLOSE;
			JDQ2 = CLOSE;
			JDQ_Run.JDQ_1_FLAG = 0;

#else

#endif

			//Trigger_1.trigger_state = 1;
		}

		if (Q2)
		{
			RemoteControl_1.QKEY_Clicked = TRUE;

			// UART1.UART_SendString("Q2 is click");
#if DIANDONG
			JDQ_Run.JDQ_2_FLAG = 1;
			JDQ1 = CLOSE;
			JDQ2 = OPEN;

			while (Q2)
			{
				Public.Delay_ms(10);
			}

			JDQ1 = CLOSE;
			JDQ2 = CLOSE;
			JDQ_Run.JDQ_2_FLAG = 0;

#else

			JDQ_Run.JDQ_2_FLAG = !JDQ_Run.JDQ_2_FLAG;
#endif

			//Trigger_1.trigger_state = 2;
		}

		if(Q4)
		{
			RemoteControl_1.QKEY_Clicked = TRUE;
			//按一下 切换系统模式
			System.ucSystem_Status = System_RUN;
		}
	}
}



/********************************************************
  End Of File
********************************************************/
