#ifndef __REMOTECONTROL_H_
#define __REMOTECONTROL_H_


#define Q1	P24
#define Q2	P23
#define Q3	P22
#define Q4	P21
#define Q5	P20
#define Q6	P44
#define Q7	P43
#define Q8	P42


typedef struct{
	//添加一个状态判断按键被按下了 从而跳出循环直接执行
	uint8_t QKEY_Clicked;					//	遥控器被按下
	void (*QKEY_Detect)(void);    			//按键检测
	void (*QKEY_StandBy_Flag_Detect)(void);    			//按键检测

}RemoteControl_t;

/* extern variables-----------------------------------------------------------*/
extern RemoteControl_t RemoteControl_1;

/* extern function prototypes-------------------------------------------------*/




#endif
/********************************************************
  End Of File
********************************************************/
