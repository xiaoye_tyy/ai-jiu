#ifndef __Modbus_H__
#define __Modbus_H__

//宏定义

//定义枚举类型

//定义结构体类型
typedef struct
{
	uint16_t Head1;                       //地址1
	uint16_t Head2;                       //地址1
	void (*Protocol_Analysis)(UART_t*);  //协议分析
} Modbus_t;

/* extern variables-----------------------------------------------------------*/
extern Modbus_t xdata  Modbus;

/* extern function prototypes-------------------------------------------------*/

#endif
/********************************************************
  End Of File
********************************************************/