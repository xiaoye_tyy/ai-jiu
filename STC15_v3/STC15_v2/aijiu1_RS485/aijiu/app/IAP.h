#ifndef __IAP_H__
#define __IAP_H__

/* define ------------------------------------------------------------------*/
#define   IAP_HIGH_TEMP_ADDR    (uint16_t)0x0000  //高温阈值储存地址
#define   IAP_LOW_TEMP_ADDR     (uint16_t)0x0200  //低温阈值储存地址
#define   IAP_USE_NUM_ADDR		(uint16_t)0x0400  //使用次数储存地址
#define   IAP_CNT    (uint8_t)10

//定义枚举类型

//定义结构体类型
typedef struct
{
	uint8_t ucIAP_Flag;    //IAP操作标志位
	uint8_t ucIAP_Cnt;     //IAP操作计数
	
	uint8_t (*IapReadByte)(uint16_t);            //读一字节  
	void    (*IapProgramByte)(uint16_t,uint8_t); //写一字节(写之前需要先擦除)
	void    (*IapEraseSector)(uint16_t);         //扇区擦除
}IAP_t;

/* extern variables-----------------------------------------------------------*/
extern IAP_t IAP;

#endif
/********************************************************
  End Of File
********************************************************/