#ifndef __UART_H_
#define __UART_H_

//定义枚举类型
typedef enum
{	
	Band_4800     = (uint8_t)0,
	Band_9600	    = (uint8_t)1,
	Band_19200	  = (uint8_t)2,
	Band_115200	  = (uint8_t)3,
}BandRate_t;

typedef enum
{	
	TTL       = (uint8_t)0,
	RS_485	  = (uint8_t)1,
	RS_232	  = (uint8_t)2,
}Interface_Type_t;

//定义异步串口结构体类型
typedef struct
{
	BandRate_t        ucBandRate;      //波特率
	uint8_t  volatile ucTX_Busy_Flag;  //发送忙碌标志
	uint8_t  volatile ucRec_Flag;      //接收标志位
	uint8_t  volatile ucRec_Cnt;       //接收计数
	
	uint8_t* pucSend_Buffer;           //发送缓存指针 
	uint8_t* pucRec_Buffer;            //接收缓存指针 
	
	void (*UART_Init)(void);           //串口初始化
	void (*UART_SendData)(uint8_t);    //串口发送字符
	void (*UART_SendArray)(uint8_t*,uint16_t);  //串口发送数组
	void (*UART_SendString)(uint8_t*); //串口发送字符串
	void (*Protocol)(void);            //接口协议

	uint8_t	Interface_Type;            //接口类型
	void (*RS485_Set_SendMode)(void);  //RS-485接口设置为发送模式
	void (*RS485_Set_RecMode)(void);   //RS-485接口设置为接收模式
	
} UART_t;

/* extern variables-----------------------------------------------------------*/

/* extern function prototypes-------------------------------------------------*/

#endif
/********************************************************
  End Of File
********************************************************/