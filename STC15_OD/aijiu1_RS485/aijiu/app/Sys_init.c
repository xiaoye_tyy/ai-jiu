/* Includes ------------------------------------------------------------------*/
#include <main.h>

/* Private define-------------------------------------------------------------*/

/* Private variables----------------------------------------------------------*/
static void GPIO_Init(void);           //通用输入输出端口初始化
static void IE_init(void);             //中断初始化
static void Power_ON_Indication(void); //上电指示
static void Sys_Init(void);            //系统初始化

/* Public variables-----------------------------------------------------------*/
Hardware_Init_t Hardware_Init = 
{
	GPIO_Init,
	IE_init,
	Sys_Init
};

/* Private function prototypes------------------------------------------------*/      

/*
	* @name   GPIO_Init
	* @brief  通用输入输出端口初始化
	* @param  None
	* @retval None      
*/
static void GPIO_Init()
{
	//M1  M0
		//0   0	     准双向口
		//0   1	     推挽输出
		//1   0	     高阻输入
		//1   1	     开漏

	//对JQD的GPIO进行配置 设置为准双向口
/*
	#define JDQ1 P00 //上
	#define JDQ2 P01	//下
	#define JDQ3 P02	//上2
	#define JDQ4 P03	//下2
	#define JDQ5 P04	//右
	#define JDQ6 P05	//左
		//220V
	#define JDQ8 P46	//风机
	#define JDQ9 P45	//风机
	#define JDQ10 P27	//加热
	#define JDQ11 P26	//点火
	#define JDQ12 P25	
	#define JDQ13 P24	
*/
	P0M1 = 0x00;//0000 0000
	P0M0 = 0x00;//0000 0000

	P2M1 = 0x00;//0000 0000
	P2M0 = 0x00;//0000 0000

	P4M1 = 0x01;
	P4M0 = 0x20;


	/*
	对于遥控器的GPIO进行设置准双向口
	#define Q1	P47
	#define Q2	P12
	#define Q3	P13
	#define Q4	P17
	#define Q5	P54
	#define Q6	P55
	#define Q7	P40
	#define Q8	P34
	*/
//	P1M1 |= 0x00;
//	P1M0 |= 0x00;
	P5M1 = 0x30;
	P5M0 = 0x00;
	//P3M1 |= 0x00;
	//P3M0 |= 0x00;
	/*
	对MAX6675GPIO设置为
	#define SCK  P15    推挽输出
	#define CS 	 P16	推挽输出
	#define MISO P14	浮空输入
	
	#串口二的设计 P10 P11
	
	P1M0 |=(0x01<<5);
	P1M0 |=(0x01<<6);
	P1M1 |=(0x01<<4);
	*/
	P1M0 = 0x62;
	P1M1 = 0x1d;
	
	P16 = 1;
	P15 = 1;
	/*
		对于串口进行GPIO引脚配置
		P30		高阻输入
		P31		推挽输出
		P35		DE 推挽输出
		P32   高阻输入
		P33 	高阻输入
		P34   高阻输入
		P36 	高阻输入
	*/
	P3M0 = 0x22;
	P3M1 = 0x5D;

	
	P00 = 0;
	P01 = 0;
	P02 = 0;
	P03 = 0;
	P04 = 0;
	P05 = 0;
	P06 = 0;

	
	P45 = 0;
	P27 = 0;
	P26 = 0;
	P25 = 0;
	P24 = 0;
	P23 = 0;
	
	
	

}

/*
	* @name   IE_init
	* @brief  中断初始化
	* @param  None
	* @retval None      
*/
static void IE_init()
{	

	IE0  = 0;            //将INT0中断请求标志位清"0"             
	//IE1 = 0;						//将INT1中断请求标志位清"0"
	
	IT0  = 0; 					//上升沿触发或者下降触发
	//IT1 = 0;			      //外部中断1的上升或者下降触发
	
	EX0  = 1;            //打开外部中断0
	//EX1 = 1;		        //打开外部中断1

	//INT_CLKO |= 0x10;  //(EX2 = 1)使能INT2中断  默认为下降沿触发
	ET0 =  1;            //打开定时器0中断
	
	ES  = 1;              //使能串口1中断
	IE2 |= BIT0;          //使能串口2中断
	
	EA  =  1;       			//打开总中断
	
	
	
}



/*
	* @name   Sys_Init
	* @brief  系统初始化
	* @param  None
	* @retval None      
*/
static void Sys_Init()
{	
	Public.Delay_ms(50);                //上电延时50ms
	Hardware_Init.GPIO_Init();          //GPIO口初始化
	Timer0.Timer0_Init();               //定时器0初始化
	Hardware_Init.IE_init();            //中断初始化
	UART1.UART_Init();                  //串口1初始化
	UART2.UART_Init();                  //串口2初始化


}

/********************************************************
  End Of File
********************************************************/