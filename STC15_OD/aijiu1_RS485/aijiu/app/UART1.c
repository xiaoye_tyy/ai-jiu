/* Includes ------------------------------------------------------------------*/
#include <main.h>

/* Private define-------------------------------------------------------------*/
#define S1_S1	 BIT7
#define S1_S0	 BIT6

#define UART1_MAX485_DE_nRE 	 P46
#define UART1_MAX485_SendMode  (bit)1
#define UART1_MAX485_RecMode   (bit)0

#define UART1_Send_LENGTH  20
#define UART1_Rec_LENGTH 	 20

/* Private variables----------------------------------------------------------*/

static uint8_t xdata ucSend_Buffer[UART1_Send_LENGTH] = {0,1,2,3,4,5,6,7,8,9};
static uint8_t xdata ucRec_Buffer [UART1_Rec_LENGTH]  = {0x00};

/* Private function prototypes------------------------------------------------*/      
static void Init(void);                    //串口初始化
static void SendData(uint8_t);             //串口发送字符
static void SendArray(uint8_t*,uint16_t);     //串口发送数组
static void SendString(uint8_t*);          //串口发送字符串
static void Protocol(void);                //串口协议

static void RS485_Set_SendMode(void); //RS-485接口设置为发送模式
static void RS485_Set_RecMode(void);  //RS-485接口设置为接收模式

/* Public variables-----------------------------------------------------------*/
UART_t idata UART1 = 
{
	Band_9600,
	
	FALSE,
	FALSE,
	0,
	
	ucSend_Buffer,
	ucRec_Buffer,
	
	Init,
	SendData,
	SendArray,
	SendString,
	Protocol,
	
	RS_485,
	RS485_Set_SendMode,
	RS485_Set_RecMode
};

/*
	* @name   Init
	* @brief  串口1初始化
	* @param  None
	* @retval None      
*/
static void Init()
{
	//串口1映射至P30,P31 不需要配置
//	AUXR1 &= (~S1_S1);//置0
//	AUXR1 |=   S1_S0;//置1
//	AUXR1 &= (~S1_S1);
//	AUXR1 &= (~S1_S0);
	SCON  = 0x50;		//8位数据,可变波特率
	AUXR |= 0x40;		//定时器1时钟为Fosc,即1T
	AUXR &= 0xFE;		//串口1选择定时器1为波特率发生器
	TMOD &= 0x0F;		//设定定时器1为16位自动重装方式
	//设定波特率
	switch(UART1.ucBandRate)
	{
		case Band_4800:    TL1 = 0x80; TH1 = 0xFB; break;
		case Band_9600:    TL1 = 0xC0; TH1 = 0xFD; break;
		case Band_19200:   TL1 = 0xE0; TH1 = 0xFE; break;
		case Band_115200:  TL1 = 0xD0; TH1 = 0xFF; break;
		default:           TL1 = 0xC0; TH1 = 0xFD; break;
	}
	ET1 = 0;		    //禁止定时器1中断
	TR1 = 1;		    //启动定时器1
	
	REN   = 1;      //允许串口1接收
}

/*
	* @name   SendData
	* @brief  发送字符
	* @param  dat:待发送字符
	* @retval None      
*/
static void SendData(uint8_t dat)
{
	while(UART1.ucTX_Busy_Flag);       //等待前面的数据发送完
	UART1.ucTX_Busy_Flag = TRUE;       //置位忙碌标志
	SBUF = dat;                        //写数据至UART寄存器
}

/*
	* @name   SendArray
	* @brief  串口发送数组
	* @param  p_Arr:数组首地址，LEN:发送长度
	* @retval None      
*/
static void SendArray(uint8_t* p_Arr,uint16_t LEN) 
{
	uint16_t i = 0;

	UART1.RS485_Set_SendMode();
	
	for(i=0;i<LEN;i++)
	{
		UART1.UART_SendData(*(p_Arr+i));
	}
	while(UART1.ucTX_Busy_Flag); //等待数据发送完
	Public.Delay_ms(1);          //等待数据传输完

	UART1.RS485_Set_RecMode();
}


/*
	* @name   SendString
	* @brief  发送字符串
	* @param  p_Str:待发送字符串
	* @retval None      
*/
static void SendString(uint8_t* p_Str) 
{	
	UART1.RS485_Set_SendMode();
	
	while(*p_Str)
	{
		UART1.UART_SendData(*(p_Str++)); //发送当前字符
	}
	while(UART1.ucTX_Busy_Flag); //等待数据发送完
	Public.Delay_ms(1);          //等待数据传输完

	UART1.RS485_Set_RecMode();
}

/*
	* @name   RS485_Set_SendMode
	* @brief  RS-485接口设置为发送模式
	* @param  None
	* @retval None      
*/
static void RS485_Set_SendMode()
{
	UART1_MAX485_DE_nRE = UART1_MAX485_SendMode;
	Public.Delay_ms(1);
}

/*
	* @name   RS485_Set_RecMode
	* @brief  RS-485接口设置为接收模式
	* @param  None
	* @retval None      
*/
static void RS485_Set_RecMode()
{
	Public.Delay_ms(1);
	UART1_MAX485_DE_nRE = UART1_MAX485_RecMode;
}


#ifdef Monitor_Run_Code
	//putchar字符发送函数重定向
	extern char putchar(char c)
	{
		UART1.UART_SendData((uint8_t)c);
		return c;
	}
#endif


/*
	* @name   UART1_isr
	* @brief  串口1中断服务函数
	* @param  None
	* @retval None      
*/
void UART1_isr() interrupt 4
{
	if(RI)
	{					
		RI = (bit)0; //清除接收中断标志
		
		if(UART1.ucRec_Cnt < UART1_Rec_LENGTH)
		{
			ucRec_Buffer[UART1.ucRec_Cnt++] = SBUF;	
		}
		
		UART1.ucRec_Flag = TRUE;
	}
	
	if(TI)
	{
		TI = (bit)0;                  //清除发送中断标志
		UART1.ucTX_Busy_Flag = FALSE; //清忙碌标志
	}
}



/*
	* @name   Protocol
	* @brief  串口协议
	* @param  None
	* @retval None      
*/
static void Protocol()
{
	if(UART1.ucRec_Flag == TRUE)
	{
		//LED_Run.LED_Flip();
		
		//过滤干扰数据低于6位的数据过滤掉
		if(ucRec_Buffer[0] != 0)
		{
			Timer0.usDelay_Timer = 0;
			while(UART1.ucRec_Cnt < 6)
			{
				//WatchDog.Feed();
				if(Timer0.usDelay_Timer >= TIMER0_100mS) //100ms没接收完6个字节，跳出循环
					break;
			}
			
		  //协议分析
			Modbus.Protocol_Analysis(&UART1);
		}
		
		//清除缓存
		Public.Memory_Clr(ucRec_Buffer,(uint16_t)UART1.ucRec_Cnt);
		
		//重新接收
		UART1.ucRec_Cnt  = 0;
		UART1.ucRec_Flag = FALSE;
	}
}
/********************************************************
  End Of File
********************************************************/