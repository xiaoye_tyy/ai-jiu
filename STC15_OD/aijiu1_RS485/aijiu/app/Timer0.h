#ifndef __Timer0_H__
#define __Timer0_H__

//定义枚举类型
typedef enum
{
	TIMER0_10mS  	= (uint16_t)2,
	TIMER0_50mS  	= (uint16_t)10,
	TIMER0_100mS	= (uint16_t)20,
	TIMER0_200mS	= (uint16_t)40,
	TIMER0_500mS	= (uint16_t)100,
	TIMER0_1S     = (uint16_t)200,
	TIMER0_2S     = (uint16_t)400,
	TIMER0_3S     = (uint16_t)600,
	TIMER0_5S     = (uint16_t)1000,
	TIMER0_30S    = (uint16_t)6000,
	TIMER0_1minute = (uint16_t)12000,
	TIMER0_2minute = (uint16_t)24000,
	TIMER0_4minute = (uint16_t)48000,
	TIMER0_5minute = (uint16_t)60000,
}TIMER0_Value_t;

//定义结构体类型
typedef struct
{
  uint16_t volatile usMCU_Run_Timer;  //系统运行定时器
	uint16_t volatile usDelay_Timer;    //延时定时器

	uint8_t  fire_Timer_start;					//点火标志
	uint16_t fire_Timer;						//点火定时器
	uint16_t WeChat_Timer;				//微信小程序更新定时器
	uint16_t temperature_Timer;			//温度更新定时器
	uint16_t Screen_Timer;				//屏幕数据更新定时器
	uint16_t Reduce_Timer;        //次数减少定时器
	uint16_t System_Run_Timer;		//系统运行定时器
	void (*Timer0_Init)(void);          //定时器0初始化
	
} Timer0_t;

/* extern variables-----------------------------------------------------------*/
extern Timer0_t  Timer0;

#endif
/********************************************************
  End Of File
********************************************************/