#ifndef __REMOTECONTROL_H_
#define __REMOTECONTROL_H_


#define Q1	P47
#define Q2	P12
#define Q3	P13
#define Q4	P17
#define Q5	P54
#define Q6	P55
#define Q7	P40
#define Q8	P34


typedef struct{
	//添加一个状态判断按键被按下了 从而跳出循环直接执行
	uint8_t QKEY_Clicked;					//	遥控器被按下
	void (*QKEY_Detect)(void);    			//按键检测


}RemoteControl_t;

/* extern variables-----------------------------------------------------------*/
extern RemoteControl_t RemoteControl_1;

/* extern function prototypes-------------------------------------------------*/




#endif
/********************************************************
  End Of File
********************************************************/
