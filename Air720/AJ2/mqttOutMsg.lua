--模块功能：MQTT客户端数据发送处理，将获取的MCU数据提取后发送给服务器
--@author：郭锋，gflytu@163.com，15653973302
--@copyright：临沂大学物联网研发中心&山东合正科技发展有限公司
-- @release 2022.01.22


module(...,package.seeall)
require "misc"
require "uart2"
--获取模组的IMEI作为mqtt订阅主题使用

--数据发送的消息队列
local msgQueue = {}

local function insertMsg(topic,payload,qos,user)
    table.insert(msgQueue,{t=topic,p=payload,q=qos,user=user})
    --sys.publish("APP_SOCKET_SEND_DATA")
end

local function pubQos1TestCb(result)
    log.info("mqttOutMsg.pubQos1TestCb",result)
    --if result then sys.timerStart(pubQos0Test,10000) end
end
local num=0
function  pubData(data)
    local DeviceName=misc.getImei()
    local t=data
    log.info("--发送数据到服务器--",t)
    insertMsg("p/"..DeviceName.."/pub",t,1,{cb=pubQos1TestCb})
    --log.info("send data:",t)
end
--初始化MQTT客户端发送数据
function init()
    log.info("--初始化完毕--")
end

--去除初始化
function unInit()
    while #msgQueue>0 do
        local outMsg=table.remove(msgQueue,1)
        if outMsg.user and outMsg.user.cb then
            outMsg.user.cb(false,outMsg.user.para)
        end
    end
end

--MQTT客户端发送数据
function proc(mqttClient)
    while #msgQueue>0 do
        local outMsg=table.remove(msgQueue,1)
        local result=mqttClient:publish(outMsg.t,outMsg.p,outMsg.q)
        log.info("--mqtt客户端发送--",outMsg.t,outMsg.p,outMsg.q)
        if outMsg.user and outMsg.user.cb then
            outMsg.user.cb(result,outMsg.user.para)
            log.info("outMSg USuer 和 user.para",outMsg.user,outMsg.user.para,outMsg.user.cb)
        end
        if not result then
            return
        end
    end
    return true
    -- body
end
sys.subscribe("UART_RECV_DATA",pubData)
--调用回调函数pubData，从而将串口接收到的数据发送给服务器
