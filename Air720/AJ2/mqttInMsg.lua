--模块功能：MQTT客户端数据发送处理，将获取的MCU数据提取后发送给服务器
--@author：郭锋，gflytu@163.com，15653973302
--@copyright：临沂大学物联网研发中心&山东合正科技发展有限公司
-- @release 2022.01.22

module(...,package.seeall)
require "uart2"
require "utils"
require "sys"
--- MQTT客户端数据接收处理
-- @param mqttClient，MQTT客户端对象
-- @return 处理成功返回true，处理出错返回false
-- @usage mqttInMsg.proc(mqttClient)
--s=""
function proc(mqttClient)
    local result
    Mdata=""
    while true do
        result,Mdata = mqttClient:receive(3000) --接收成功result为true
        --接收到数据判断及处理
        if result then
            log.info("--MQTT 接收数据--",Mdata.topic,Mdata.payload)
            --s=Mdata.payload
             sys.publish("MQTT_RECV_DATA",Mdata.payload)
        else
            break
        end
    end

    return result or Mdata
end
