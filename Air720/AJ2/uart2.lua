--- 模块功能：串口2测试（与MCU通信）
-- @author 郭锋(gflytu@163.com，15653973302)
-- @module uart2测试
-- @copyright 临沂大学物联网研发中心
-- @release 2022.01.23
require "utils"
require "pm"
require "pins"
require "misc"
module(..., package.seeall)
dateRE={}
--串口ID,2对应uart2，如果要修改为uart1，把UART_ID赋值为1即可
local UART_ID = 1
function write(s)
    log.info("向MCU发送数据为：",s)
    uart.write(UART_ID,s.."\r\n")
end
function uartData(reData)
    write(reData)
end

local function writeOk()
    log.info("testUartTask.writeOk")
end
local function Uart2Read()
    local MCUData=""
    while true do
        local s = uart.read(UART_ID,"*l")
        if s == "" then
           -- uart.on(UART_ID,"receive",function() sys.publish("UART_RECEIVE") end)
            if not sys.waitUntil("UART_RECEIVE",100) then
                --uart接收数据，如果100毫秒没有收到数据，则打印出来所有已收到的数据，清空数据缓冲区，等待下次数据接收
                --注意：
                --串口帧没有定义结构，仅靠软件延时，无法保证帧的完整性，如果对帧接收的完整性有严格要求，必须自定义帧结构（参考testUart.lua）
                --因为在整个GSM模块软件系统中，软件定时器的精确性无法保证，例如本demo配置的是100毫秒，在系统繁忙时，实际延时可能远远超过100毫秒，达到200毫秒、300毫秒、400毫秒等
                --设置的延时时间越短，误差越大
                if MCUData:len()>0 then
                    --log.info("testUartTask.taskRead","100ms no data, received length",MCUData:len())
                    --数据太多，如果全部打印，可能会引起内存不足的问题，所以此处仅打印前1024字节
                    --log.info("testUartTask.taskRead","received data",MCUData:sub(1,1024))
                    MCUData = ""
                    -- = frameCnt+1
                end
            end
        else
            MCUData = MCUData..s
            ----[[
                local MCUString
            MCUString=string.toHex(MCUData)
            local _,n= string.find(MCUString,"FF5505022306")
            ----]]
            if n~=nil then
                local imei=misc.getImei()
            --for i=1,string.len(imei) then
                write(imei)
                log.info("接收到MCU数据正确：",MCUString)
            else
                log.info("接收到的MCU数据错误：",MCUString)
                sys.publish("UART_RECV_DATA",MCUData)
            end
            --发布主题UART_RECV_DATA和流转的数据MCUData，该数据由mcu通过串口发送给模组720sg，由订阅该主题的回调函数使用数据
        end
    end
end

--[[
函数名：write
功能  ：通过串口发送数据
参数  ：s：要发送的数据
返回值：无
]]


--[[function imeitomcu()
    local imei = misc.getImei()
    Imei=""..imei
    t={}
    s = ""
    for i=1,string.len(imei) do
        t[i] = string.format("%x",string.sub(Imei,i,i))
        i=i+1
        --print(t[11])
        --s = s..' '..'0'..t[1]
    end
    for j=1,string.len(imei),1 do
        s = s..' '..'0'..t[j]
        end
    print("ff 55 05 "..s)
        uart.write( UART_ID,"ff5505 ".. Imei)
end]]
--sys.timerLoopStart(imeitomcu,10000)

--保持系统处于唤醒状态，此处只是为了测试需要，所以此模块没有地方调用pm.sleep("testUartTask")休眠，不会进入低功耗休眠状态
--在开发“要求功耗低”的项目时，一定要想办法保证pm.wake("testUartTask")后，在不需要串口时调用pm.sleep("testUartTask")
pm.wake("usart2")
--注册串口的数据发送通知函数
--uart.on(UART_ID,"sent",writeOk)
--配置并且打开串口
uart.setup(UART_ID,115200,8,uart.PAR_NONE,uart.STOP_1)
--如果需要打开“串口发送数据完成后，通过异步消息通知”的功能，则使用下面的这行setup，注释掉上面的一行setup
--uart.setup(UART_ID,115200,8,uart.PAR_NONE,uart.STOP_1,nil,1)
--启动串口数据接收任务
sys.taskInit(Uart2Read)
sys.subscribe("MQTT_RECV_DATA",uartData)
--订阅回调函数主题MQTT_RECV_DATA，流转到回调函数uartData中，然后将MQTT接收到的数据通过串口发送给mcu，由mcu进行下一步处理
