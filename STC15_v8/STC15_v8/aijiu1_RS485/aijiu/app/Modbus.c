

/* Includes ------------------------------------------------------------------*/
#include <main.h>

/* Private define-------------------------------------------------------------*/
#define FunctionCode_Read_Register 0x03	//读寄存器
#define FunctionCode_Write_Register 0x06	//写寄存器
#define FunctionCode_Write_Imei 0x05	//写Imei码
#define FunctionCode_Boot_The_Device 0x0a	//启动设备
#define FunctionCode_Heat_Ignition 0x0b 	//加热点火
#define FunctionCode_Use_The_Device 0x0c	//使用设备


/* Private variables----------------------------------------------------------*/
/* Private function prototypes------------------------------------------------*/
static void Protocol_Analysis(UART_t *); //协议分析
static void Modbus_Read_Register(UART_t *); //读寄存器
static void Modbus_Wrtie_Register(UART_t *); //写寄存器
static void Modbus_Write_Imei(UART_t *); //写Imei寄存器
static void Modbus_Boot_The_Device(UART_t * UART); //启动关闭设备
static void Modbus_Heat_Ignition(UART_t * UART); //启动关闭设备

static void Modbus_Use_The_Device(UART_t * UART); //使用设备



/* Public variables-----------------------------------------------------------*/
Modbus_t xdata	Modbus =
{
	0xFF, 
	0x55, 
	Protocol_Analysis
};


/*
	* @name   Protocol_Analysis
	* @brief  协议分析
	* @param  UART -> 串口指针
	* @retval None		
*/
static void Protocol_Analysis(UART_t * UART)
{
	UART_t * const xdata COM = UART;
	uint8_t i = 0;

	//校验地址
	if (((* (COM->pucRec_Buffer + 0)) == Modbus.Head1) && ((* (COM->pucRec_Buffer + 1)) == Modbus.Head2))
		{
		//处理数据 根据功能码对寄存器进行读写操作
		switch ((* (COM->pucRec_Buffer + 2)))
			{
			case FunctionCode_Read_Register:
				Modbus_Read_Register(COM);
				break;

			case FunctionCode_Write_Register:
				Modbus_Wrtie_Register(COM);
				break;

			case FunctionCode_Write_Imei:
				Modbus_Write_Imei(COM);
				break;

			case FunctionCode_Boot_The_Device:
				Modbus_Boot_The_Device(COM);
				break;

			case FunctionCode_Heat_Ignition:
				Modbus_Heat_Ignition(COM);
				break;

			case FunctionCode_Use_The_Device:
				Modbus_Use_The_Device(COM);

			default:
				break;
			}
		}
		if(((* (COM->pucRec_Buffer + 0)) == '8') && ((* (COM->pucRec_Buffer + 1)) == '6'))
		{
			Modbus_Write_Imei(COM);
	
		}
}


/*
	* @name   Modbus_Read_Register
	* @brief  读寄存器
	* @param  UART -> 串口指针
	* @retval None		
*/
static void Modbus_Read_Register(UART_t * UART)
{
	UART_t * const xdata COM = UART;

	////回应数据
	//地址码
	* (COM->pucSend_Buffer + 0) = Modbus.Head1;
	* (COM->pucSend_Buffer + 1) = Modbus.Head2;

	//功能码
	* (COM->pucSend_Buffer + 2) = FunctionCode_Read_Register;

	//数据长度
	* (COM->pucSend_Buffer + 3) = 15;

	//温度
	* (COM->pucSend_Buffer + 4) = 0;
	* (COM->pucSend_Buffer + 5) = temperature_t.temperature;

	//继电器
	* (COM->pucSend_Buffer + 6) = 0;
	* (COM->pucSend_Buffer + 7) = JDQ_Run.JDQ_1_FLAG;
	* (COM->pucSend_Buffer + 8) = JDQ_Run.JDQ_2_FLAG;
	* (COM->pucSend_Buffer + 9) = JDQ_Run.JDQ_3_FLAG;
	* (COM->pucSend_Buffer + 10) = JDQ_Run.JDQ_4_FLAG;
	* (COM->pucSend_Buffer + 11) = JDQ_Run.JDQ_5_FLAG;
	* (COM->pucSend_Buffer + 12) = JDQ_Run.JDQ_6_FLAG;
	* (COM->pucSend_Buffer + 13) = JDQ_Run.JDQ_8_FLAG;
	* (COM->pucSend_Buffer + 14) = JDQ_Run.JDQ_9_FLAG;
	* (COM->pucSend_Buffer + 15) = JDQ_Run.JDQ_10_FLAG;
	* (COM->pucSend_Buffer + 16) = JDQ_Run.JDQ_11_FLAG;
	* (COM->pucSend_Buffer + 17) = JDQ_Run.JDQ_11_FLAG;
	* (COM->pucSend_Buffer + 18) = JDQ_Run.JDQ_11_FLAG;


	//发送数据
	//
	//UART1.UART_SendArray(COM->pucSend_Buffer,19);
}


/*
	* @name   Modbus_Read_Register
	* @brief  写寄存器
	* @param  UART -> 串口指针
	* @retval None		
*/
static void Modbus_Wrtie_Register(UART_t * UART)
{
	UART_t * const xdata COM = UART;
	uint8_t 		i	= 0;
	uint16_t high = 0;
	uint16_t low  = 0;
	////回应数据
	//准备数据
	//	for(i=0;i<8;i++)
	//	{
	//		*(COM->pucSend_Buffer+i) = *(COM->pucRec_Buffer+i);
	//	}
	//	//发送数据
	//	UART1.UART_SendArray(COM->pucSend_Buffer,8);
	//写继电器
	switch (* (COM->pucRec_Buffer + 4))
		{
		case 0x01:
			JDQ_Run.JDQ_1_FLAG = * (COM->pucRec_Buffer + 5) - 5;
			Trigger_1.trigger_state = 1;
			break;

		case 0x02:
			JDQ_Run.JDQ_2_FLAG = * (COM->pucRec_Buffer + 5) - 5;
			Trigger_1.trigger_state = 2;
			break;

		case 0x03:
			JDQ_Run.JDQ_3_FLAG = * (COM->pucRec_Buffer + 5) - 5;
			Trigger_1.trigger_state = 3;
			break;

		case 0x04:
			JDQ_Run.JDQ_4_FLAG = * (COM->pucRec_Buffer + 5) - 5;
			Trigger_1.trigger_state = 4;
			break;

		case 0x05:
			JDQ_Run.JDQ_5_FLAG = * (COM->pucRec_Buffer + 5) - 5;
			Trigger_1.trigger_state = 5;
			break;

		case 0x06:
			JDQ_Run.JDQ_6_FLAG = * (COM->pucRec_Buffer + 5) - 5;
			Trigger_1.trigger_state = 6;
			break;

		case 0x07:
			break;

		case 0x08:
			JDQ_Run.JDQ_8_FLAG = * (COM->pucRec_Buffer + 5) - 5;
			Trigger_1.trigger_state = 8;
			break;

		case 0x09:
			JDQ_Run.JDQ_9_FLAG = * (COM->pucRec_Buffer + 5) - 5;
			Trigger_1.trigger_state = 9;
			break;

		case 0x0a:
			JDQ_Run.JDQ_10_FLAG = * (COM->pucRec_Buffer + 5) - 5;
			Trigger_1.trigger_state = 10;
			break;

		case 0x0b:
			JDQ_Run.JDQ_11_FLAG = * (COM->pucRec_Buffer + 5) - 5;
			Trigger_1.trigger_state = 11;
			break;

		case 0x0c:
			JDQ_Run.JDQ_12_FLAG = * (COM->pucRec_Buffer + 5) - 5;
			Trigger_1.trigger_state = 12;
			break;

		case 0x0d:
			JDQ_Run.JDQ_13_FLAG = * (COM->pucRec_Buffer + 5) - 5;
			Trigger_1.trigger_state = 13;
			break;

		//高温阈值
		
		case 0x0f:
			//从串口获取高温阈值
			high = * (COM->pucRec_Buffer + 5);
			low = * (COM->pucRec_Buffer + 6);
			temperature_t.hightemp = (high<<8)|low;
			if(temperature_t.hightemp>=5){
				temperature_t.lowtemp = temperature_t.hightemp -5;
			}
			UART1.UART_SendString(" The high temperature threshold setting is complete\n");
			printf("high temperature is %d",temperature_t.hightemp );
			printf("low		temperature is %d",temperature_t.lowtemp );
			break;
			
		//低温阈值
		
		case 0x10:
			high = * (COM->pucRec_Buffer + 5);
			low = * (COM->pucRec_Buffer + 6);
			firTempearture = (high<<8)|low;
			UART1.UART_SendString(" The fire temperature threshold setting is complete\n");
			printf("fire temperature is %d",firTempearture);

			break;

		//使用次数
		case 0x11:

			break;

		default:
			break;
		}

	//Modbus_Read_Register(COM);
}


/*
	* @name   Modbus_Write_Imer
	* @brief  将收到的Imer数据直接通过串口1发送给屏幕
	* @param  UART -> 串口指针
	* @retval None		
*/
static void Modbus_Write_Imei(UART_t * UART)
{
	UART_t * const xdata COM = UART;
	uint8_t 		i	= 0;

	////回应数据
	//准备数据
	
	* (COM->pucSend_Buffer + 0) = Modbus.Head1;
	* (COM->pucSend_Buffer + 1) = Modbus.Head2;
	
		//功能码
	* (COM->pucSend_Buffer + 2) = FunctionCode_Write_Imei;
	
		//数据长度
	* (COM->pucSend_Buffer + 3) = 15;

	for(i = 0;i<15;i++)
	{
		*(COM->pucSend_Buffer + 4+i) = *(COM->pucRec_Buffer + i);
	}

	UART1.UART_SendArray(COM->pucSend_Buffer, 19);

	StandBy_Flag = 1;
	
	DataUpdate(2);
	
	Public.Delay_ms(1000);
	
	//发送数据
}


/*
	* @name   Modbus_Boot_The_Device
	* @brief  ff550a010e05/06 控制设备的启动

	* @param  UART -> 串口指针
	* @retval None		
*/
static void Modbus_Boot_The_Device(UART_t * UART)
{
	UART_t * const xdata COM = UART;

	if ((* (COM->pucRec_Buffer + 4)) == 0x0e)
		{
		JDQ_Run.JDQ_8_FLAG	= * (COM->pucRec_Buffer + 5) - 5;
		Trigger_1.trigger_state = 14;
		}
}


/*
	* @name   Modbus_Boot_The_Device
	* @brief  
	| ff	 | 55	| 0b   | 01   | 0f	 | 06[点火] | 	 |
	
	* @param  UART -> 串口指针
	* @retval None		
*/
static void Modbus_Heat_Ignition(UART_t * UART)
{
	UART_t * const xdata COM = UART;

	if ((* (COM->pucRec_Buffer + 4)) == 0x0f)
		{
		JDQ_Run.JDQ_11_FLAG = * (COM->pucRec_Buffer + 5) - 5;
		Trigger_1.trigger_state = 11;
		}
}




/*
	* @name   Modbus_Use_The_Device
	* @brief  | ff	 | 55	| 0c   | 01   | 20	 | 06[启用] | 	
	* @param  UART -> 串口指针
	* @retval None		
*/
static void Modbus_Use_The_Device(UART_t * UART)
{
	UART_t * const xdata COM = UART;
	uint8_t 		i	= 0;

	if ((* (COM->pucRec_Buffer + 4)) == 0x20)
		{
		if ((* (COM->pucRec_Buffer + 5)) == 0x06)
			{
			System.ucSystem_Status = 1;
			}
		else 
			{
			System.ucSystem_Status = 0;
			}
		}

	//收到数据，给串口1反馈 用同一个数据即可
	for (i = 0; i < 6; i++)
		{
		* (COM->pucSend_Buffer + i) = * (COM->pucRec_Buffer + i);
		}

	//发送数据
	UART1.UART_SendArray(COM->pucSend_Buffer, 6);
	
	DataUpdate(2);
	

	}


/********************************************************
  End Of File
********************************************************/
