/* Includes ------------------------------------------------------------------*/
#include <main.h>

/* Private define-------------------------------------------------------------*/
#define		S2TI	  BIT1     //S2CON.1
#define		S2RI	  BIT0     //S2CON.0
#define 	S2REN   BIT4     //S2CON.4

#define UART2_Send_LENGTH  10
#define UART2_Rec_LENGTH 	 100

/* Private variables----------------------------------------------------------*/

static uint8_t xdata ucSend_Buffer[UART2_Send_LENGTH] = {0x00};
static uint8_t xdata ucRec_Buffer [UART2_Rec_LENGTH]  = {0x00};

/* Private function prototypes------------------------------------------------*/      
static void Init(void);                    //串口初始化
static void SendData(uint8_t);             //串口发送字符
static void SendArray(uint8_t*,uint16_t);  //串口发送数组
static void SendString(uint8_t*);          //串口发送字符串
static void Protocol(void);                //串口协议

static void RS485_Set_SendMode(void); //RS-485接口设置为发送模式
static void RS485_Set_RecMode(void);  //RS-485接口设置为接收模式

/* Public variables-----------------------------------------------------------*/
UART_t idata UART2 = 
{
	Band_115200,
	
	FALSE,
	FALSE,
	0,
	
	ucSend_Buffer,
	ucRec_Buffer,
	
	Init,
	SendData,
	SendArray,
	SendString,
	Protocol,
	
	TTL,
	RS485_Set_SendMode,
	RS485_Set_RecMode
};

/*
	* @name   Init
	* @brief  串口2初始化
	* @param  None
	* @retval None      
*/
static void Init()
{
	S2CON = 0x50;		//8位数据,可变波特率
	AUXR |= 0x04;		//定时器2时钟为Fosc,即1T
	//设定波特率
	switch(UART2.ucBandRate)
	{
		case Band_4800:    T2L = 0xA0; T2H = 0xFF; break;
		case Band_9600:    T2L = 0xC0; T2H = 0xFD; break;
		case Band_19200:   T2L = 0xE0; T2H = 0xFE; break;
		case Band_115200:  T2L = 0xD0; T2H = 0xFF; break;
		default:           T2L = 0xC0; T2H = 0xFD; break;
	}
	AUXR |= 0x10;		 //启动定时器2
	
	S2CON |= S2REN;  //允许串口2接收
}

/*
	* @name   SendData
	* @brief  发送字符
	* @param  dat:待发送字符
	* @retval None      
*/
static void SendData(uint8_t dat)
{
	while(UART2.ucTX_Busy_Flag);       //等待前面的数据发送完
	UART2.ucTX_Busy_Flag = TRUE;       //置位忙碌标志
	S2BUF = dat;                        //写数据至UART寄存器
}

/*
	* @name   SendArray
	* @brief  串口发送数组
	* @param  p_Arr:数组首地址，LEN:发送长度
	* @retval None      
*/
static void SendArray(uint8_t* p_Arr,uint16_t LEN) 
{
	uint16_t i = 0;

	UART2.RS485_Set_SendMode();
	
	for(i=0;i<LEN;i++)
	{
		UART2.UART_SendData(*(p_Arr+i));
	}
	while(UART2.ucTX_Busy_Flag); //等待数据发送完
	Public.Delay_ms(1);          //等待数据传输完

	UART2.RS485_Set_RecMode();
}


/*
	* @name   SendString
	* @brief  发送字符串
	* @param  p_Str:待发送字符串
	* @retval None      
*/
static void SendString(uint8_t* p_Str) 
{	
	UART2.RS485_Set_SendMode();
	
	while(*p_Str)
	{
		UART2.UART_SendData(*(p_Str++)); //发送当前字符
	}
	while(UART2.ucTX_Busy_Flag); //等待数据发送完
	Public.Delay_ms(1);          //等待数据传输完

	UART2.RS485_Set_RecMode();
}


/*
	* @name   RS485_Set_SendMode
	* @brief  RS-485接口设置为发送模式
	* @param  None
	* @retval None      
*/
static void RS485_Set_SendMode()
{
	
}

/*
	* @name   RS485_Set_RecMode
	* @brief  RS-485接口设置为接收模式
	* @param  None
	* @retval None      
*/
static void RS485_Set_RecMode()
{
	
}

/*
	* @name   UART2_isr
	* @brief  串口1中断服务函数
	* @param  None
	* @retval None      
*/
void UART2_isr() interrupt 8
{
	//接收
	if(S2CON & S2RI)
	{
		S2CON &= ~S2RI;     //清除接收中断标志	
		
		if(UART2.ucRec_Cnt < UART2_Rec_LENGTH)
		{
			ucRec_Buffer[UART2.ucRec_Cnt++] = S2BUF;	
		}
		
		UART2.ucRec_Flag = TRUE;
	}
	
	//发送
	if(S2CON & S2TI)
	{
		S2CON &= ~S2TI;   //清除发送中断标志
		UART2.ucTX_Busy_Flag = FALSE; //清忙碌标志
	}
}


/*
	* @name   Protocol
	* @brief  串口协议
	* @param  None
	* @retval None      
*/
static void Protocol()
{
	if(UART2.ucRec_Flag == TRUE)
	{
		//LED_Run.LED_Flip();
		
		//过滤干扰数据0
		if(ucRec_Buffer[0] != 0)
		{
			Timer0.usDelay_Timer = 0;
			while(UART2.ucRec_Cnt < 6)
			{
				//WatchDog.Feed();
				if(Timer0.usDelay_Timer >= TIMER0_100mS) //100ms没接收完6个字节，跳出循环
					break;
			}
			Modbus.Protocol_Analysis(&UART2);
			
		}
		
		Public.Memory_Clr(ucRec_Buffer,(uint16_t)UART2.ucRec_Cnt);
		//重新接收
		UART2.ucRec_Cnt  = 0;
		UART2.ucRec_Flag = FALSE;
	}
		
	
}
/********************************************************
  End Of File
********************************************************/