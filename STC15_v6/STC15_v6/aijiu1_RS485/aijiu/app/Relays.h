#ifndef __RELAYS_H_
#define __RELAYS_H_

//JDQ
#define 	JDQ1 	P07 //上
#define		JDQ2 	P06	//下
//虚拟的实际上没有用
#define 	JDQ3 	P00	//上2
#define 	JDQ4 	P46	//下2
#define 	JDQ5 	P45	//右
#define 	JDQ6 	P35	//左
//220V
#define 	JDQ8 	P47	//加热
#define 	JDQ9 	P12	//风扇
#define 	JDQ10 	P13	//风机
#define 	JDQ11 	P14	//点火
#define 	JDQ12 	P15	//点火
#define 	JDQ13 	P16	//点火


//RUN JDQ

#define   MCU_Run_JDQ_ON 	       (bit)1
#define   MCU_Run_JDQ_OFF        	(bit)0

/* Includes ------------------------------------------------------------------*/

typedef struct
{
	uint8_t JDQ_1_FLAG;
	uint8_t JDQ_2_FLAG;
	uint8_t JDQ_3_FLAG;
	uint8_t JDQ_4_FLAG;
	uint8_t JDQ_5_FLAG;
	uint8_t JDQ_6_FLAG;
	uint8_t JDQ_7_FLAG;
	uint8_t JDQ_8_FLAG;
	uint8_t JDQ_9_FLAG;
	uint8_t JDQ_10_FLAG;
	uint8_t JDQ_11_FLAG;
	uint8_t JDQ_12_FLAG;
	uint8_t JDQ_13_FLAG;
} JDQS_t;



/* extern variables-----------------------------------------------------------*/
extern JDQS_t idata JDQ_Run;
/* extern function prototypes-------------------------------------------------*/ 

#endif
/********************************************************
  End Of File
********************************************************/
