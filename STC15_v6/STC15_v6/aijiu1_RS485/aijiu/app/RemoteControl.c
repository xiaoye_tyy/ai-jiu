

/* Includes ------------------------------------------------------------------*/
#include <main.h>

/* Private define-------------------------------------------------------------*/
#define DIANDONG 1

/* Private variables----------------------------------------------------------*/
/* Private function prototypes------------------------------------------------*/
static void QKEY_Detect(void); //按钮开关检测
static void	QKEY_StandBy_Flag_Detect(void);// 模式切换按键检测
/* Public variables-----------------------------------------------------------*/
//结构体初始化
RemoteControl_t RemoteControl_1 =
	{
		FALSE, QKEY_Detect,QKEY_StandBy_Flag_Detect};

/*
 * @name   KEY_Detect
 * @brief  按钮开关检测
 * @param  None
 * @retval None
 */
static void QKEY_Detect()
{
	//单击检测
	if (Q1 || Q2 || Q4 )
	{
		//消抖
		Public.Delay_ms(250);

		if (Q1)
		{
			RemoteControl_1.QKEY_Clicked = TRUE;

			// UART1.UART_SendString("Q1 is click");
			
			JDQ_Run.JDQ_1_FLAG = 1;
			JDQ1 = OPEN;
			JDQ2 = CLOSE;
			LED3_MTR = ON;
			while (Q1)
			{
				Public.Delay_ms(200);
			}
			LED3_MTR = OFF;
			
			JDQ1 = CLOSE;
			JDQ2 = CLOSE;
			JDQ_Run.JDQ_1_FLAG = 0;


		}

		if (Q2)
		{
			RemoteControl_1.QKEY_Clicked = TRUE;

			// UART1.UART_SendString("Q2 is click");

			JDQ_Run.JDQ_2_FLAG = 1;
		
			JDQ1 = CLOSE;
			JDQ2 = OPEN;
			LED3_MTR = ON;
			while (Q2)
			{
				Public.Delay_ms(200);
			}
			
			LED3_MTR = OFF;
			JDQ1 = CLOSE;
			JDQ2 = CLOSE;
			JDQ_Run.JDQ_2_FLAG = 0;


		}

//		if (Q3)
//		{

//			//一键启动
//			RemoteControl_1.QKEY_Clicked = TRUE;
//			JDQ_Run.JDQ_8_FLAG = 1;
//			Trigger_1.trigger_state = 14;


//		}
		
		if (Q4)
		{
			
			//一键关闭
			RemoteControl_1.QKEY_Clicked = TRUE;
			JDQ_Run.JDQ_8_FLAG = 0;
			Trigger_1.trigger_state = 14;
		}

	}
}



static void	QKEY_StandBy_Flag_Detect(void)
{
	
	if (Q1||Q2||Q3||Q4)
		{
			//消抖
			Public.Delay_ms(250);
			if (Q1)
		{
			RemoteControl_1.QKEY_Clicked = TRUE;

			// UART1.UART_SendString("Q1 is click");

			JDQ_Run.JDQ_1_FLAG = 1;
			JDQ1 = OPEN;
			JDQ2 = CLOSE;
			LED3_MTR = ON;
			

			while (Q1)
			{
				Public.Delay_ms(200);
			}
			
			LED3_MTR = OFF;
			JDQ1 = CLOSE;
			JDQ2 = CLOSE;
			JDQ_Run.JDQ_1_FLAG = 0;


		}

		if (Q2)
		{
			RemoteControl_1.QKEY_Clicked = TRUE;

			JDQ_Run.JDQ_2_FLAG = 1;
			JDQ1 = CLOSE;
			JDQ2 = OPEN;
			LED3_MTR = ON;
			
			while (Q2)
			{
				Public.Delay_ms(200);
			}
			
			LED3_MTR = OFF;
			JDQ1 = CLOSE;
			JDQ2 = CLOSE;
			JDQ_Run.JDQ_2_FLAG = 0;



			//Trigger_1.trigger_state = 2;
		}
		//非运行模式，点击一键启动，进入运行模式
		if(Q3)
		{
				
			RemoteControl_1.QKEY_Clicked = TRUE;
			JDQ_Run.JDQ_8_FLAG = 1;
			Trigger_1.trigger_state = 14;
			StandBy_Flag = 1;
			System.ucSystem_Status = System_RUN;
			TM1650_Set(0x48,0x01);//打开显示
		}
		//非运行模式 按键4关闭所有设施
		if(Q4)
		{
			RemoteControl_1.QKEY_Clicked = TRUE;
			//按一下 切换系统模式
			JDQ9 = CLOSE; 
			JDQ10 = CLOSE;
			LED2_FAN = OFF;//风扇、风机指示灯
			
			JDQ_Run.JDQ_10_FLAG = 0;
			JDQ_Run.JDQ_9_FLAG = 0;
			TM1650_Set(0x48,0x00);//关闭显示
			
			
			
			//UART1.UART_SendString("-----散热模式-----\n");
			//StandBy_Flag = 1;
			//System.ucSystem_Status = System_RUN;
			
			
			
		}
	}
}



/********************************************************
  End Of File
********************************************************/
