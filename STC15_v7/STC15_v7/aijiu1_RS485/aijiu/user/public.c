/* Includes ------------------------------------------------------------------*/
#include <main.h>

/* Private define-------------------------------------------------------------*/
#define MAIN_Fosc		22118400L	//定义主时钟
/* Private variables----------------------------------------------------------*/
static void Delay_ms(uint16_t);               //ms延时函数
static void SPI_delay();                      //SPI延时
static void Memory_Clr(uint8_t* pucBuffer,uint16_t LEN);//内存清除
/* Public variables-----------------------------------------------------------*/
Public_t  Public = 
{
	Delay_ms,
	SPI_delay,
	Memory_Clr
};

/* Private function prototypes------------------------------------------------*/

/*
	* @name   Delay_ms
	* @brief  毫秒延时函数
	* @param  ms -> 需要延时的时间
	* @retval None      
*/
static void Delay_ms(uint16_t ms)
{
	uint16_t i;
	do{
	      i = MAIN_Fosc / 13000;
		  while(--i)	;   //14T per loop
     }while(--ms);
			
}

static void SPI_delay()
{

	unsigned char i;

	_nop_();
	_nop_();
	_nop_();
	i = 52;
	while (--i);
	
	
}

/*
	* @name   Memory_Set
	* @brief  内存清除函数
	* @param  pucBuffer -> 内存首地址
						LEN       -> 内存长度   
	* @retval None      
*/
static void Memory_Clr(uint8_t* pucBuffer,uint16_t LEN)
{
	uint16_t i;
	
	for(i=0;i<LEN;i++)
	{
		*(pucBuffer+i) = (uint8_t)0;
	}
}

/********************************************************
  End Of File
********************************************************/