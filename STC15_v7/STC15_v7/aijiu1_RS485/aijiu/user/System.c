

/* Includes ------------------------------------------------------------------*/
#include <main.h>


#define FIRE_TEMP 				250		//点火启动温度
#define RUN_TIME_ONE			12    //单次运行时间12*5=60分钟
/* Private variables----------------------------------------------------------*/



const uint8_t code ReadIMEI[6] = {0xff,0x55,0x05,0x02,0x23,0x06};
const uint8_t code ReduceUsage[19] = {0xff,0x55,0x09,};
unsigned char code CODE[11]= {0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f,0x00};//0-9,灭
unsigned char  code CODED[11]= {0xbf,0x86,0xdb,0xcf,0xe6,0xed,0xfd,0x87,0xff,0xdf,0x00};//0-9,灭
unsigned char code SEG[4]={0x68,0x6a,0x6c,0x6e};//四位数码管从1-4的地址
    
uint8_t Fire_Timer_Count = 0;
uint8_t Sys_Timer_Flag = 0;
uint8_t OneKey = 0;
uint8_t OneKey_Fire = 0;
uint16_t firTempearture = FIRE_TEMP;

/* Private function prototypes------------------------------------------------*/
static void Run(void); //系统运行
static void StandBy(void); //系统待机
static void Halt(void); //停机模式

void sys_run_update(); //数据更新
void sys_run_check();//状态监测





/* Public variables-----------------------------------------------------------*/
System_t		System =
{
	0, //默认为待机模式
	Run, 
	StandBy 										//Halt
};


static uint16_t count = 0;
/*
	* @name   Run
	* @brief  系统运行
	* @param  None
	* @retval None		
*/
static void Run()
{


	sys_run_check();
	//不再解析串口信息，设置为遥控器直接控制
	UART1.Protocol();								//串口1协议解析
	UART2.Protocol();								//串口2协议解析
	Trigger_1.Trigger_Control();		// 只要触发，就会更新状态
	sys_run_update();
	
}


/*
	* @name   Run
	* @brief  系统信息上传更新
	* @param  None
	* @retval None		
*/
void sys_run_update()
{

	switch (StateFlag)
		{
		//监测状态
		case 0:
				break;
		//串口1
		case 1:
			StateFlag = 0;
			break;

		//串口2
		case 2:
			StateFlag = 0;
			DataUpdate(3);
			break;

		//遥控器
		case 3:
			StateFlag = 0;
			DataUpdate(3);
			break;

		//定时更新屏幕
		case 4:
			StateFlag = 0;
			
			DataUpdate(1);
			break;

		//定时更新微信小程序
		case 5:
			StateFlag = 0;
			DataUpdate(2);
			break;
		default:
			break;
		}



}


/*
	* @name   StandBy
	* @brief  系统待机模式，监控串口中断
	* @param  None
	* @retval None		
*/
static void StandBy(void)
{
	uint16_t i	=  1000;
	
	while(i--)
	{
		
			//打开遥控器检测 检测Q8
		RemoteControl_1.QKEY_StandBy_Flag_Detect();
		
		Public.Delay_ms(1);
	
		if(RemoteControl_1.QKEY_Clicked == TRUE)
		{
			RemoteControl_1.QKEY_Clicked = FALSE;
			break;
		}
			if (UART1.ucRec_Flag == TRUE)
			{
			printf("recive data from uart1\n");
			break;
			}

		if (UART2.ucRec_Flag == TRUE)
			{
			printf("recive data from uart2\n");
			break;
			}
		count++;
		//更新温度
		if(count==1000)
		{
			count = 0;
			temperature_t.temperature = (uint16_t)temperature_t.max6675_readTemperature();
			if(temperature_t.temperature>=40)
			{
				temperature_t.temperature = temperature_t.temperature*120/100;
			}

				LEDNUM_UpData(temperature_t.temperature);
			
		}
		
		if(Timer0.WeChat_Timer>=TIMER0_10S)
		{
				DataUpdate(3);
				Timer0.WeChat_Timer = 0;
		}
		
	}
	//监测串口信息，更新System系统状态，当系统更改了状态
	UART1.Protocol();								//串口1协议解析
	UART2.Protocol();								//串口2协议解析
	switch(StandBy_Flag)
	{

		case 1:
			
			//进入空闲模式更新数据
			
			break;
		case 2:
			//一键关闭进入散热模式
			if(Timer0.temperature_Timer>=TIMER0_2minute)
			{
				JDQ9 = CLOSE;
				JDQ10 = CLOSE;
				LED2_FAN = OFF;
				
				JDQ_Run.JDQ_10_FLAG = 0;
				JDQ_Run.JDQ_9_FLAG = 0;
				//UART1.UART_SendString("-----散热模式-----\n");
				StandBy_Flag = 1;
				TM1650_Set(0x48,0x00);//初始化为6级灰度，开显示
				
			
			}

			break;
		default:
			break;
		
	}
}

//类似中断的方式 及时更新设备的运行状态与温度信息

void sys_run_check()
{
	static uint16_t i	= 0;
	uint16_t temp = 0;
	//延时500ms
	i	= 1000;	
	while (i--)
		{		
		RemoteControl_1.QKEY_Detect();
		Public.Delay_ms(1);
		count++;
		if(count==1000)
		{
			count = 0;
			temperature_t.temperature = (uint16_t)temperature_t.max6675_readTemperature();
		
			
			//打开220V温度补偿加10
			//temperature_t.temperature = temperature_t.temperature+10;
			//高温补偿
			
			
			if(temperature_t.temperature>=40)
			{
				temperature_t.temperature = temperature_t.temperature*120/100;
			}
			

			LEDNUM_UpData(temperature_t.temperature);

		}
		//遥控器是否触发
		if (RemoteControl_1.QKEY_Clicked == TRUE)
			{
			RemoteControl_1.QKEY_Clicked = FALSE;
			StateFlag			= 3;
			break;
			}
			
			if (UART1.ucRec_Flag == TRUE)
			{
			StateFlag			= 1;
				printf("recive data from uart1\n");
			break;
			}

		if (UART2.ucRec_Flag == TRUE)
			{
			StateFlag			= 2;
			printf("recive data from uart2\n");
			break;
			}

			
			//系统运行状态定时器 
			//当定时时间为60分钟时进入系统待机模式
			//此时更改状态为 待机状态  减少一次使用次数
			//待机定时器数据置零

			
			if(Timer0.System_Run_Timer>=TIMER0_5minute)
			{
				Timer0.System_Run_Timer = 0;
				Sys_Timer_Flag++;//计数加1
			}
			
			if(Sys_Timer_Flag>=RUN_TIME_ONE)
			{
				Sys_Timer_Flag = 0;
				JDQ_Run.JDQ_8_FLAG = 0;
				//进入待机模式
				UART2.UART_SendArray(ReduceUsage,19);//发送减少指令
				Public.Delay_ms(500);//延迟500ms
				
				Trigger_1.trigger_state = 14;
				break;
			}
			
		//点火定时器 
		if (Timer0.fire_Timer >= TIMER0_1minute30s)
			{
			//触发后将定时器重置为0;
			Timer0.fire_Timer	= 0;
			Timer0.fire_Timer_start = FALSE;
			Trigger_1.trigger_state = 12;
			break;
			}
		if (Timer0.WeChat_Timer >= TIMER0_10S)
			{
			StateFlag			= 5;
			Timer0.WeChat_Timer = 0;
			break;
			}
			
			// 开机后 打开一键启动才进行自动加热
			if(OneKey)
			{
				if ((temperature_t.temperature > 0) && (temperature_t.temperature < 500))
					{
						//进入一键启动后，如果温度大于190度则启动点火
					if((temperature_t.temperature>=FIRE_TEMP)&&(OneKey_Fire==0))
						{
							JDQ_Run.JDQ_11_FLAG = 1;
							Trigger_1.trigger_state = 11;
							StateFlag	= 3;
							OneKey_Fire = 1;
							break;
						}
					
						//如果加热没开且温度小于预设的温度此时打开加热
					if ((JDQ_Run.JDQ_8_FLAG==0)&&(temperature_t.temperature <= temperature_t.lowtemp))
						{
							printf("low temp:%d",temperature_t.lowtemp);
							JDQ_Run.JDQ_8_FLAG	= 1;
							Trigger_1.trigger_state = 8;
							StateFlag			= 3;
							break;
						}
					//如果加热已开且温度大于阈值 则关闭加热
					if ((JDQ_Run.JDQ_8_FLAG==1)&&(temperature_t.temperature >= temperature_t.hightemp))
					{
						printf("high temp:%d",temperature_t.hightemp);
						JDQ_Run.JDQ_8_FLAG	= 0;
						Trigger_1.trigger_state = 8;
						StateFlag	= 3;
						break;
					}
					}
				}	
				
			if(Timer0.Screen_Timer >= TIMER0_2S)
			{
				StateFlag			= 4;
				Timer0.Screen_Timer = 0;
				printf("---low temp:%d",temperature_t.lowtemp);
				printf("---high temp:%d",temperature_t.hightemp);
				
				break;
			}
		}
}



void LEDNUM_UpData( uint16_t Data)
{
	uint8_t i = 0;
	if(Data<=999&&Data>99)
		{
			TM1650_Set(SEG[0],CODE[10]);
			i=(Data/100)%10;
			TM1650_Set(SEG[1],CODE[i]);
			i=(Data/10)%10;
			TM1650_Set(SEG[2],CODE[i]);
			i=Data%10;
			TM1650_Set(SEG[3],CODE[i]);
		}
		if(Data<=99&&Data>9)
		{
			TM1650_Set(SEG[0],CODE[10]);
			TM1650_Set(SEG[1],CODE[10]);
			i=(Data/10)%10;
			TM1650_Set(SEG[2],CODE[i]);
			i=Data%10;
			TM1650_Set(SEG[3],CODE[i]);
		}
		if(Data<=9&&Data>=0)
		{
			TM1650_Set(SEG[0],CODE[10]);
			TM1650_Set(SEG[1],CODE[10]);
			TM1650_Set(SEG[2],CODE[10]);
			i=Data%10;
			TM1650_Set(SEG[3],CODE[i]);
		}
	
}


