#ifndef __MAIN_H_
#define __MAIN_H_

/* Includes ------------------------------------------------------------------*/
#include <STC15.h>
#include <stdio.h>
#include <string.h>
#include <intrins.h>
#include <public.h>
#include <Sys_init.h>
#include <Timer0.h>
#include <Relays.h>
#include <UART.h>
#include <UART1.h>
#include <UART2.h>
#include <RemoteControl.h>
#include <Trigger.h>
#include <MAX6675.h>
#include <Modbus.h>
#include <tm1650.h>
#include <System.h>
/* extern variables-----------------------------------------------------------*/
#define OPEN 				0
#define CLOSE       1

#define ON          1
#define OFF					0

//#define LED1_HOT  			P05
//#define LED2_FAN 				P04
//#define LED3_MTR 				P03	
//#define LED4_FIR 				P02
//#define LED5_RUN 				P01
//#define LED6_POW 				P17



#define LED1_HOT  			P01
#define LED2_FAN 				P17
#define LED3_MTR 				P03	
#define LED4_FIR 				P02
#define LED5_RUN 				P04
#define LED6_POW 				P05

/* extern function prototypes-------------------------------------------------*/ 
void DataUpdate(uint8_t);
extern uint8_t StateFlag;
extern uint8_t StandBy_Flag;

extern uint8_t Fire_Timer_Count ;
extern uint8_t Sys_Timer_Flag;
extern uint8_t code close_device[6];
extern uint8_t OneKey;
extern uint8_t OneKey_Fire;
#endif
/********************************************************
  End Of File
********************************************************/