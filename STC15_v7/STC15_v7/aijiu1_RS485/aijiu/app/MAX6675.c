/* Includes ------------------------------------------------------------------*/
#include <main.h>

/* Private define-------------------------------------------------------------*/
#define TEMP  26
#define LOWTEMP  319
#define HIGHTEMP  320



/* Private variables----------------------------------------------------------*/

/* Private function prototypes------------------------------------------------*/  
static uint8_t max6675_readWriteByte(uint8_t txData);
static uint16_t max6675_readRawValue(void);
static uint16_t max6675_readTemperature(void);






/* Public variables-----------------------------------------------------------*/
Temperature_t temperature_t = 
{
	TEMP,
	HIGHTEMP,
	LOWTEMP,
	
	
	max6675_readWriteByte,
	max6675_readRawValue,
	max6675_readTemperature,
	
};


/* Private function prototypes------------------------------------------------*/  


static uint8_t max6675_readWriteByte(uint8_t txData)
{
		unsigned char temp=0;
    unsigned char dat=0;
    for(temp=0x80; temp!=0; temp>>=1)
    {
        SCK=0;
        Public.SPI_delay();  //让SCK稳定
        SCK=1;
        Public.SPI_delay();  //让SCK稳定
        if(MISO==1)
        {
            dat|=temp;
        }
        else
        {
            dat&=~temp;
        }
    }
    return dat;
	
}

static uint16_t max6675_readRawValue(void)
{
    uint16_t tmp=0;
	
		//减少因为定时中断引起的时序问题，降低温度波动，暂停定时器
		TR0 = 0;//关闭定时器0
    //GPIO_ResetBits(MAX6675_CS_PORT,MAX6675_CS_PIN); //enable max6675
		CS = 0;
    tmp=max6675_readWriteByte(0XFF); //read MSB
    tmp <<= 8;
    tmp |= max6675_readWriteByte(0XFF); //read LSB;
    //tmp=tmp&0x00FF;
    //GPIO_SetBits(MAX6675_CS_PORT,MAX6675_CS_PIN); //disable max6675
		CS = 1;
    if (tmp & 4)
    {
        // thermocouple open
        //tmp = 0x0FFF; //未检测到热电偶	4095
		tmp = 4095; //未检测到热电偶	4095
		
    }
    else
    {
        tmp = tmp >> 3;
    }
		tmp=tmp&0x0FFF;	//12bit
		TR0 = 1;//打开定时器0
    return tmp;
}

static uint16_t max6675_readTemperature(void)
{
	return (uint16_t)(max6675_readRawValue() * 1023.75 / 4096);
}









/********************************************************
  End Of File
********************************************************/


