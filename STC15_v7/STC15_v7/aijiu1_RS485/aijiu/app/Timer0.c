/* Includes ------------------------------------------------------------------*/
#include <main.h>

/* Private define-------------------------------------------------------------*/

/* Private variables----------------------------------------------------------*/

/* Private function prototypes------------------------------------------------*/      
static void Timer0_Init(void);//定时器0初始化

/* Public variables-----------------------------------------------------------*/
Timer0_t  Timer0 = 
{
	0,
	0,
	FALSE,
	0,
	0,
	0,
	0,
	0,
	Timer0_Init
};


/*
	* @name   Timer0_Init
	* @brief  定时器0初始化
	* @param  None
	* @retval None      
*/
static void Timer0_Init() //5毫秒@22.1184MHz
{

	AUXR &= 0x7F;		//定时器时钟12T模式
	TMOD &= 0xF0;		//设置定时器模式
	TL0 = 0x00;		//设置定时初始值
	TH0 = 0xDC;		//设置定时初始值
	TF0 = 0;		//清除TF0标志
	TR0 = 1;		//定时器0开始计时

}

/*
	* @name   Timer0_isr()
	* @brief  定时器0中断函数(5ms中断一次)
	* @param  None-
	* @retval None      
*/
/***********定时器0中断函数***********/
void Timer0_isr() interrupt 1
{ 
	
	if(++Timer0.usMCU_Run_Timer >= TIMER0_1S)
	{
		Timer0.usMCU_Run_Timer = 0;
	}
	Timer0.usDelay_Timer++; 
	//点火延时定时器
	if(Timer0.fire_Timer_start==1)
	{
		Timer0.fire_Timer++;//点火定时	
	}
	Timer0.WeChat_Timer++;
	Timer0.temperature_Timer++;
	Timer0.Screen_Timer++;

	//系统运行定时器 只有系统处于运行模式 该定时器才计数
	if(System.ucSystem_Status==1)
	{
		Timer0.System_Run_Timer++;
	}
	
	
}
/********************************************************
  End Of File
********************************************************/