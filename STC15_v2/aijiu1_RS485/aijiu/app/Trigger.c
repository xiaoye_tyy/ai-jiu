

/* Includes ------------------------------------------------------------------*/
#include <main.h>

/* Private define-------------------------------------------------------------*/

/* Private variables----------------------------------------------------------*/
static void Trigger_Control(void);
uint8_t code	close_device[6] =
{
	0xff, 0x55, 0x0C, 0x02, 0x20, 0x05
};


/* Public variables-----------------------------------------------------------*/
Trigger_t		Trigger_1 =
{

	0, 
	Trigger_Control
};



/* Private function prototypes------------------------------------------------*/
/*
	* @name   Trigger_Control
	* @brief  根据触发器对JDQ标志进行操作
	* @param  None
	* @retval None		
*/
static void Trigger_Control(void)
{


	if (Trigger_1.trigger_state != 0)
		{

		switch (Trigger_1.trigger_state)
			{
			case 1:
				JDQ_Run.JDQ_2_FLAG = 0;

				if (JDQ_Run.JDQ_1_FLAG)
					{
					JDQ1				= OPEN;
					JDQ2				= CLOSE;
					}
				else 
					{
					JDQ1				= CLOSE;
					JDQ2				= CLOSE;
					JDQ_Run.JDQ_1_FLAG	= 0;
					}

				Trigger_1.trigger_state = 0;
				break;

			case 2:
				JDQ_Run.JDQ_1_FLAG = 0;

				if (JDQ_Run.JDQ_2_FLAG)
					{
					JDQ1				= CLOSE;
					JDQ2				= OPEN;
					}
				else 
					{
					JDQ1				= CLOSE;
					JDQ2				= CLOSE;
					JDQ_Run.JDQ_2_FLAG	= 0;
					}

				Trigger_1.trigger_state = 0;
				break;

			case 3:
				JDQ_Run.JDQ_4_FLAG = 0;

				if (JDQ_Run.JDQ_3_FLAG)
					{
					JDQ3				= OPEN;
					JDQ4				= CLOSE;
					StrokeSwitch1.Key1_Flag = FALSE;
					}
				else 
					{
					JDQ3				= CLOSE;
					JDQ4				= CLOSE;
					JDQ_Run.JDQ_3_FLAG	= 0;
					}

				Trigger_1.trigger_state = 0;
				break;

			case 4:
				JDQ_Run.JDQ_3_FLAG = 0;

				if (JDQ_Run.JDQ_4_FLAG && (StrokeSwitch1.Key1_Flag == FALSE))
					{
					JDQ3				= CLOSE;
					JDQ4				= OPEN;
					}
				else 
					{
					JDQ3				= CLOSE;
					JDQ4				= CLOSE;
					JDQ_Run.JDQ_4_FLAG	= 0;
					}

				Trigger_1.trigger_state = 0;
				break;

			case 5:

				JDQ_Run.JDQ_6_FLAG = 0;
				if (JDQ_Run.JDQ_5_FLAG && (StrokeSwitch1.Key2_Flag == FALSE))
					{
					JDQ5				= OPEN;
					JDQ6				= CLOSE;
					}
				else 
					{
					JDQ5				= CLOSE;
					JDQ6				= CLOSE;
					JDQ_Run.JDQ_5_FLAG	= 0;
					}

				Trigger_1.trigger_state = 0;
				break;

			case 6:
				JDQ_Run.JDQ_5_FLAG = 0;

				if (JDQ_Run.JDQ_6_FLAG)
					{
					JDQ5				= CLOSE;
					JDQ6				= OPEN;
					StrokeSwitch1.Key2_Flag = FALSE;
					}
				else 
					{
					JDQ5				= CLOSE;
					JDQ6				= CLOSE;
					JDQ_Run.JDQ_6_FLAG	= 0;
					}

				Trigger_1.trigger_state = 0;
				break;

			case 7:
				break;

			case 8:
				if (JDQ_Run.JDQ_8_FLAG)
					{
					JDQ8				= OPEN;
					}
				else 
					{
					JDQ8				= CLOSE;
					}

				Trigger_1.trigger_state = 0;
				break;

			case 9:
				if (JDQ_Run.JDQ_9_FLAG)
					{
					JDQ9				= OPEN;
					}
				else 
					{
					JDQ9				= CLOSE;
					}

				Trigger_1.trigger_state = 0;
				break;

			case 10:
				if (JDQ_Run.JDQ_10_FLAG)
					{
					JDQ10				= OPEN;
					}
				else 
					{
					JDQ10				= CLOSE;
					}

				Trigger_1.trigger_state = 0;
				break;

			case 11:
				if (JDQ_Run.JDQ_11_FLAG)
					{

					Timer0.fire_Timer	= 0;
					JDQ11				= OPEN;
					JDQ12				= OPEN;
					JDQ13				= OPEN;
					Timer0.fire_Timer_start = TRUE;

					//点火间隔时间
					Fire_Timer_Count	= 1;

					}
				else 
					{

					JDQ11				= CLOSE;
					JDQ12				= CLOSE;
					JDQ13				= CLOSE;

					//关闭点火标志归0
					Fire_Timer_Count	= 0;
					}

				Trigger_1.trigger_state = 0;
				break;

			case 12:
				JDQ_Run.JDQ_11_FLAG = 0;
				JDQ11 = CLOSE;
				JDQ12 = CLOSE;
				JDQ13 = CLOSE;
				Trigger_1.trigger_state = 0;
				break;

			case 14:
				//启动、关闭设备
				// 风扇 风机 加热
				if (JDQ_Run.JDQ_8_FLAG)
					{
					JDQ8				= OPEN;
					JDQ9				= OPEN;
					JDQ10				= OPEN;
					JDQ_Run.JDQ_9_FLAG	= 1;
					JDQ_Run.JDQ_10_FLAG = 1;
					OneKey				= 1;
					}
				else 
					{
					JDQ8				= CLOSE;
					System.ucSystem_Status = System_StandBy; //系统进入待机
					Sys_Timer_Flag		= 0;
					Timer0.System_Run_Timer = 0;
					UART1.UART_SendArray(close_device, 6);
					Timer0.temperature_Timer = 0;
					StandBy_Flag		= 2;		//进入散热模式
					OneKey				= 0;
					}
				Trigger_1.trigger_state = 0;
				break;

			case 15: //关闭外设	
				break;
			default:
				break;
			}

		if (StateFlag == 1)
			{
			DataUpdate(2);
			}
		}
}


