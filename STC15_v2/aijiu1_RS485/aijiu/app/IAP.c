/* Includes ------------------------------------------------------------------*/
#include <main.h>

/* Private define-------------------------------------------------------------*/
//宏定义
#define CMD_IDLE	   0	   //空闲模式
#define CMD_READ	   1	   //IAP字节读命令
#define CMD_PROGRAM	 2	   //IAP字节编程命令
#define CMD_ERASE	   3	   //IAP扇区擦除命令
#define ENABLE_IAP	 0x83  //使能IAP，设置等待时间

/* Private variables----------------------------------------------------------*/

/* Private function prototypes------------------------------------------------*/      
static uint8_t IapReadByte(uint16_t);            //读一字节  
static void    IapProgramByte(uint16_t,uint8_t); //写一字节(写之前需要先擦除)
static void    IapEraseSector(uint16_t);         //扇区擦除

/* Public variables-----------------------------------------------------------*/
IAP_t IAP = 
{
	FALSE,
	0,
	
	IapReadByte,
	IapProgramByte,
	IapEraseSector
};

/*
	* @name   IapIdle
	* @brief  关闭IAP
	* @param  None
	* @retval None      
*/
static void IapIdle()
{
	 IAP_CONTR	= 0;	  //关闭IAP功能
	 IAP_CMD	  = 0;	  //清除命令寄存器
	 IAP_TRIG	  = 0;	  //清除触发寄存器
	 IAP_ADDRH	= 0xFF;	//将地址设置到非IAP区域
	 IAP_ADDRL	= 0xFF;
}

/*
	* @name   IapReadByte
	* @brief  读取一字节
	* @param  None
	* @retval None      
*/
static uint8_t IapReadByte(uint16_t addr)
{
	uint8_t dat;
	
	IAP_CONTR	= ENABLE_IAP;	 //使能IAP
	IAP_CMD		= CMD_READ;		 //设置IAP命令
	IAP_ADDRL	= addr;			   //设置IAP低地址
  IAP_ADDRH	= addr>>8;	   //设置IAP高地址
	IAP_TRIG	= 0x5a;			   //写触发命令
  IAP_TRIG	= 0xa5;
	_nop_();					       //等待操作完成
	dat = IAP_DATA;				   //读数据
	IapIdle();		           //关闭IAP功能

	return dat;					     //返回数据
}

/*
	* @name   IapProgramByte
	* @brief  写入一字节(写入之前需要先擦除)
	* @param  None
	* @retval None      
*/
static void IapProgramByte(uint16_t addr,uint8_t dat)
{
	IAP_CONTR	= ENABLE_IAP;	 //使能IAP
	IAP_CMD		= CMD_PROGRAM; //设置IAP命令
	IAP_ADDRL	= addr;			   //设置IAP低地址
  IAP_ADDRH	= addr>>8;		 //设置IAP高地址
	IAP_DATA	= dat;			   //写数据
	IAP_TRIG	= 0x5a;			   //写触发命令
  IAP_TRIG	= 0xa5;
	_nop_();					       //等待操作完成
	IapIdle();		           //关闭IAP功能
}

/*
	* @name   IapEraseSector
	* @brief  扇区擦除(每扇区为512字节)
	* @param  None
	* @retval None      
*/
void IapEraseSector(uint16_t addr)
{
	IAP_CONTR	= ENABLE_IAP;	 //使能IAP
	IAP_CMD		= CMD_ERASE;	 //设置IAP命令
	IAP_ADDRL	= addr;			   //设置IAP低地址
  IAP_ADDRH	= addr>>8;		 //设置IAP高地址
	IAP_TRIG	= 0x5a;			   //写触发命令
  IAP_TRIG	= 0xa5;
	_nop_();					       //等待操作完成
	IapIdle();		           //关闭IAP功能
}
/********************************************************
  End Of File
********************************************************/