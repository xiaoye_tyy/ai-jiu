#ifndef __System_H__
#define __System_H__

#define  System_StandBy 	(uint8_t)0
#define  System_RUN     	(uint8_t)1

//定义结构体类型
typedef struct
{
	uint8_t  ucSystem_Status;  //系统状态
	
	void (*Run)(void);         //系统运行
	void (*StandBy)(void);     //系统待机
//	void (*Halt)(void);        //停机模式
} System_t;

/* extern variables-----------------------------------------------------------*/
extern System_t  System;

/* extern function prototypes-------------------------------------------------*/

#endif
/********************************************************
  End Of File
********************************************************/