

/* Includes ------------------------------------------------------------------*/
#include <main.h>




/* Private variables----------------------------------------------------------*/
/* Private function prototypes------------------------------------------------*/
static void Run(void); //系统运行
static void StandBy(void); //系统待机
static void Halt(void); //停机模式
void sys_run_update(); //数据更新
void sys_run_check();//状态监测
const uint8_t code ReadIMEI[6] = {0xff,0x55,0x05,0x02,0x23,0x06};
const uint8_t code ReduceUsage[19] = {0xff,0x55,0x09,};
uint8_t Fire_Timer_Count = 0;
uint8_t Sys_Timer_Flag = 0;
uint8_t OneKey = 0;

/* Public variables-----------------------------------------------------------*/
System_t		System =
{
	1, //默认为待机模式
	Run, 
	StandBy 										//Halt
};

static uint16_t count = 0;
/*
	* @name   Run
	* @brief  系统运行
	* @param  None
	* @retval None		
*/
static void Run()
{
	
	sys_run_check();
	UART1.Protocol();								//串口1协议解析
	UART2.Protocol();								//串口2协议解析
	Trigger_1.Trigger_Control();		// 只要触发，就会更新状态
	sys_run_update();
	
}


/*
	* @name   Run
	* @brief  系统信息上传更新
	* @param  None
	* @retval None		
*/
void sys_run_update()
{

	switch (StateFlag)
		{
		//监测状态
		case 0:
				break;
		//串口1
		case 1:
			StateFlag = 0;
			break;

		//串口2
		case 2:
			StateFlag = 0;
			DataUpdate(3);
			break;

		//遥控器
		case 3:
			StateFlag = 0;
			DataUpdate(3);
			break;

		//定时更新屏幕
		case 4:
			StateFlag = 0;
			DataUpdate(1);
			break;

		//定时更新微信小程序
		case 5:
			StateFlag = 0;
			DataUpdate(2);
			break;
		default:
			break;
		}



}


/*
	* @name   StandBy
	* @brief  系统待机模式，监控串口中断，还需要测温，当温度降低到40摄氏度后才关闭风机风扇
	* @param  None
	* @retval None		
*/
static void StandBy(void)
{
	uint16_t i	=  500;
	while(i--)
	{
		Public.Delay_ms(1);
		if (UART1.ucRec_Flag == TRUE)
		{
			break;
		}	

		if (UART2.ucRec_Flag == TRUE)
		{
			break;
		}
		if(Timer0.WeChat_Timer>=TIMER0_30S)
		{
				DataUpdate(3);
				Timer0.WeChat_Timer = 0;
		}
	}
	//监测串口信息，更新System系统状态，当系统更改了状态

	UART1.Protocol();								//串口1协议解析
	UART2.Protocol();								//串口2协议解析
	switch(StandBy_Flag)
	{
		case 0:
			//获取IMEI 模式 5S发一次
			//延时等待AIR配置 配置完成后发送读取IMEI指令
			if(Timer0.WeChat_Timer>=TIMER0_5S)
			{
				UART2.UART_SendArray(ReadIMEI,6);
				Public.Delay_ms(50);
				Timer0.WeChat_Timer = 0;
			}
			break;
			
		case 1:
			//进入空闲模式更新数据
			
			break;
		case 2:
			//一键关闭进入散热模式
			if(Timer0.temperature_Timer>=TIMER0_2minute)
			{
			JDQ9 = CLOSE;
			JDQ10 = CLOSE;
			JDQ_Run.JDQ_10_FLAG = 0;
			JDQ_Run.JDQ_9_FLAG = 0;
			//UART1.UART_SendString("-----散热模式-----\n");
			StandBy_Flag = 1;
			
			}

			break;
		
	}
}

//类似中断的方式 及时更新设备的运行状态与温度信息

void sys_run_check()
{
	static uint16_t i	= 0;
	//延时500ms
	i	= 500;	
	while (i--)
		{		
		RemoteControl_1.QKEY_Detect();
		Public.Delay_ms(1);
		count++;
		if(count==500)
		{
			count = 0;
			temperature_t.temperature = (uint16_t)temperature_t.max6675_readTemperature();
			//使用屏幕温度补偿加5
			
			//打开220V温度补偿加10
			temperature_t.temperature = temperature_t.temperature+10;
			
		}
		//遥控器是否触发
		if (RemoteControl_1.QKEY_Clicked == TRUE)
			{
			RemoteControl_1.QKEY_Clicked = FALSE;
			StateFlag			= 3;
			break;
			}
		if (UART1.ucRec_Flag == TRUE)
			{
			StateFlag			= 1;
			break;
			}

		if (UART2.ucRec_Flag == TRUE)
			{
			StateFlag			= 2;
			break;
			}
			//次数减少定时器任务
			if(Timer0.Reduce_Timer>=TIMER0_5minute)
			{
				Timer0.Reduce_Timer = 0;
				Fire_Timer_Count++;//计数加1
			}
			//两次点火之间温度超过10分钟 则发送减少指令
			if(Fire_Timer_Count==3)
			{
					UART2.UART_SendArray(ReduceUsage,19);//发送减少指令
					Public.Delay_ms(500);//延迟500ms
					Fire_Timer_Count = 0;
			}
			
			//系统运行状态定时器 
			//当定时时间为45分钟时进入系统待机模式
			//此时更改状态为 待机状态
			//待机定时器数据置零
			if(Timer0.System_Run_Timer>=TIMER0_5minute)
			{
				Timer0.System_Run_Timer = 0;
				Sys_Timer_Flag++;//计数加1
			}
			
			if(Sys_Timer_Flag==9)
			{
				
				JDQ_Run.JDQ_8_FLAG = 0;
				//进入待机模式
				Trigger_1.trigger_state = 14;
				break;
			}
			
		//点火定时器 
		if (Timer0.fire_Timer >= TIMER0_4minute)
			{
			//触发后将定时器重置为0;
			Timer0.fire_Timer	= 0;
			Timer0.fire_Timer_start = FALSE;
			Trigger_1.trigger_state = 12;
			break;
			}
		if (Timer0.WeChat_Timer >= TIMER0_30S)
			{
			StateFlag			= 5;
			Timer0.WeChat_Timer = 0;
			break;
			}
			
			// 开机后 打开一键启动才进行自动加热
			if(OneKey)
			{
				if ((temperature_t.temperature > 0) && (temperature_t.temperature < 255))
					{
						//如果加热没开且温度小于预设的温度此时打开加热
					if ((JDQ_Run.JDQ_8_FLAG==0)&&(temperature_t.temperature <= temperature_t.lowtemp))
						{
							JDQ_Run.JDQ_8_FLAG	= 1;
							Trigger_1.trigger_state = 8;
							StateFlag			= 3;
							break;
						}
					//如果加热已开且温度大于阈值 则关闭加热
					if ((JDQ_Run.JDQ_8_FLAG==1)&&(temperature_t.temperature >= temperature_t.hightemp))
					{
						JDQ_Run.JDQ_8_FLAG	= 0;
						Trigger_1.trigger_state = 8;
						StateFlag	= 3;
						break;
					}
					}
				}	
				
			if(Timer0.Screen_Timer >= TIMER0_1S)
			{
				StateFlag			= 4;
				Timer0.Screen_Timer = 0;
				break;
			}
		}
}



