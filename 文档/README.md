# 艾灸控制器



## 说明：

![image-20220824100114657](.\assets\image-20220824100114657.png)

**本项目是一个的小型艾灸控制器，目前集成12个继电器、433Mhz遥控收发模块、`zksw`串口屏、4G模块，MAX6675+K型电热偶方案和微信小程序。**

![image-20220824105658340](.\assets\image-20220824105658340.png)

目前实现的功能：

- 输入信号：
  - 8路遥控器模组无线控制（点动）
  - `zksw`串口屏使用TTL或者RS485方案串口通信（点动）
  - 微信小程序的4G远程控制（按键控制、不建议使用远程电机控制）
  - MAX6675+K型电热偶测温数据
  - 行程开关信号（中断）

- 输出与控制:
  - STC15通过驱动ULN2003A从而驱动继电器
  - 6组继电器采用交换电流的方向从而实现直流电机正反转，从而实现装置的垂直、摇摆方向控制
  - 6组220V继电器分别控制加热、风扇、风机、3*点燃
  - 输出设备当前的状态信息分发给屏幕、微信小程序及时更新状态信息。（采用点动控制，遥控器触发无法显示电机驱动的信息）
- 通信协议
  - 屏幕、STC15、MQTT共用一套伪Modbus通信协议，从而实现对寄存器的读写操作。

![image-20220824093428155](D:\Code\AiJiu\assets\image-20220824093428155.png)

## 1.硬件设计

### 关于芯片选型

设计中没有直接驱动屏幕，而是采用串口屏，通过简单的串口通信实现人机交互，所以对于MCU的要求比较低了

选用**STC15W4K56S4**

- 工作电压：2.4-5.5V

- 程序空间：56k

- SRAM：4096

- EEPROM：3K

  

## 2.软件设计

软件设计分为单片机设计与串口屏幕的设计，采用模块化的编程，驱动好移植，串口屏幕参考官方历程设计。

### 2.1单片机

整体代码结构如下

```c
|----app
	|---MAX6675 测温模块
    |---Modbus  通信协议，对串口消息进行解析
    |---Relays 继电器控制
    |---RemoteControl 遥控器控制
    |---Sys_init 系统初始化 初始化GPIO、定时器、串口等配置
    |---Timer0 定时器0 自定义定时如点火时间、串口收发延时
    |---Trigger 触发器 根据触发标志位对JDQ进行控制
    |---UART1 与串口屏进行通信
    |---UART2 与4G模块进行通信
|-----obj
    |hex 下载文件
|----user
    |---main 主程序
    |---public 公用的程序 如软件延时
    |---STC15.h STC15的头文件 
```



#### **程序移植**

**快速上手**

程序移植主要根据系统的频率，串口等信息，自动生成程序，放入对应串口、定时器、软件延时文件中，下面演示的是使用ISP工具生成UART1的配置代码（配置选好，记得点击生成代码）。  

![image-20220824110358111](.\assets\image-20220824110358111.png)

```c
static void Init()
{
	//串口1映射至P30,P31 不需要配置
//	AUXR1 &= (~S1_S1);//置0
//	AUXR1 |=   S1_S0;//置1
//	AUXR1 &= (~S1_S1);
//	AUXR1 &= (~S1_S0);
	SCON  = 0x50;		//8位数据,可变波特率
	AUXR |= 0x40;		//定时器1时钟为Fosc,即1T
	AUXR &= 0xFE;		//串口1选择定时器1为波特率发生器
	TMOD &= 0x0F;		//设定定时器1为16位自动重装方式
	//设定波特率
	switch(UART1.ucBandRate)
	{
		case Band_4800:    TL1 = 0x80; TH1 = 0xFB; break;
		case Band_9600:    TL1 = 0xC0; TH1 = 0xFD; break;
		case Band_19200:   TL1 = 0xE0; TH1 = 0xFE; break;
		case Band_115200:  TL1 = 0xD0; TH1 = 0xFF; break;
		default:           TL1 = 0xC0; TH1 = 0xFD; break;
	}
	ET1 = 0;		    //禁止定时器1中断
	TR1 = 1;		    //启动定时器1
	
	REN   = 1;      //允许串口1接收
}
```

#### 通信协议

协议的制定是项目中最中要的地方，在设计之初串口、4G和STC15共用一套通信协议可以大大降低开发的难度，本项目中，STC15作为主机，串口屏幕与4G模块作为从机，STC15定时给屏幕、4G模块定时更新设备信息，屏幕、遥控器、微信小程序发送控制命令，只要改变了当前状态，STC15就会发送反馈及时更新设备状态。

**读寄存器**

```c
FF 55 03 0F 00 18 00 01 00 00 00 00 00 00 00 00 00 00 00
```

| 帧头 | 帧头 | 功能码 | 数据长度 | 分割 | 温度 | 分割 | JDQ1 | JDQ2 | JDQ3 | JDQ4 | JDQ5 | JDQ6 | JDQ8 | JDQ9 | JDQ10 | JDQ11 | JDQ12 | JDQ13 |
| ---- | ---- | ------ | -------- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ----- | ----- | ----- | ----- |
| FF   | 55   | 03     | 0F       | 00   | 18   | 00   | 00   | 00   | 00   | 00   | 00   | 00   | 00   | 00   | 00    | 00    | 00    | 00    |

格式为hex格式

JDQ 00为开 01 为闭合

```c
// 1:	串口1 单独发
// 2：串口2 单独发
// 3：串口1与2 都发
void DataUpdate(uint8_t flag)
{
	uint8_t Flag = flag;
		//地址码
	dataUpdate_Buffer[0] = Modbus.Head1;   
	dataUpdate_Buffer[1] = Modbus.Head2;
	//功能码
	dataUpdate_Buffer[2] = 0x03;
	//数据长度
	dataUpdate_Buffer[3] = 15;
	//温度
	dataUpdate_Buffer[4] = 0;
	dataUpdate_Buffer[5] = temperature_t.temperature;
	//继电器
	dataUpdate_Buffer[6] = 0;
	dataUpdate_Buffer[7] = JDQ_Run.JDQ_1_FLAG;
	dataUpdate_Buffer[8] = JDQ_Run.JDQ_2_FLAG;
	dataUpdate_Buffer[9] = JDQ_Run.JDQ_3_FLAG;
	dataUpdate_Buffer[10] = JDQ_Run.JDQ_4_FLAG;
	dataUpdate_Buffer[11]	= JDQ_Run.JDQ_5_FLAG;
	dataUpdate_Buffer[12]	= JDQ_Run.JDQ_6_FLAG;
	
	dataUpdate_Buffer[13]	= JDQ_Run.JDQ_8_FLAG;
	dataUpdate_Buffer[14]	= JDQ_Run.JDQ_9_FLAG;
	dataUpdate_Buffer[15]	= JDQ_Run.JDQ_10_FLAG;
	dataUpdate_Buffer[16]	= JDQ_Run.JDQ_11_FLAG;//三个加热棒是同时启动关闭
	dataUpdate_Buffer[17]	= JDQ_Run.JDQ_11_FLAG;
	dataUpdate_Buffer[18]	= JDQ_Run.JDQ_11_FLAG;
	
	//将更新的数据发送给
	
	switch(flag)
	{
		case 1:
			UART1.UART_SendArray(dataUpdate_Buffer,19);
			break;
		case 2:
			UART2.UART_SendArray(dataUpdate_Buffer,19);
			break;
		case 3:
			UART2.UART_SendArray(dataUpdate_Buffer,19);
			UART1.UART_SendArray(dataUpdate_Buffer,19);
			break;
		default:
			break;		
	}
	Public.Memory_Clr(dataUpdate_Buffer,20);//清除数据
	
}

```



**写寄存器**

|       写寄存器       |  ff  |  55  |   06   |    01    |     01     | 05/06[闭/开] |
| :------------------: | :--: | :--: | :----: | :------: | :--------: | :----------: |
|         功能         | 帧头 | 帧头 | 功能码 | 数据长度 | 寄存器地址 |     数据     |
|    启动/关闭设备     |  ff  |  55  |   0a   |    01    |     0e     | 05/06[闭/开] |
|       启动点火       |  ff  |  55  |   0b   |    01    |     0f     |   06[点火]   |
|       使用设备       |  ff  |  55  |   0c   |    01    |     20     |    05/06     |
|       写入使用       |  ff  |  55  |   0c   |    02    |     20     |    05/06     |
| 减少使用次数 | ff | 55 | 09 | 00 | 00 |  |
| 写入高温阈值 | ff | 55 | 06 | 01 | 0f | 温度值 |
| 写入低温阈值 | ff | 55 | 06 | 01 | 10 | 温度值 |
| 读取IMEI | ff | 55 | 05 | 02 | 23 | 06 |








| 寄存器地址 |     寄存器      |
| :--------: | :-------------: |
|    0x01    |  JDQ1   上升1   |
|    0x02    |  JDQ2    下降1  |
|    0x03    |  JDQ3    上升2  |
|    0x04    |  JDQ4    下降2  |
|    0x05    |   JDQ5     左   |
|    0x06    |   JDQ6     右   |
|    0x08    |  JDQ8     加热  |
|    0x09    |  JDQ9     风机  |
|    0x0a    |  JDQ10   风扇   |
|    0x0b    | JDQ11  加热棒1  |
|    0x0c    | JDQ12   加热棒2 |
|    0x0d    | JDQ13   加热棒3 |
|    0x0e    |  启动/关闭设备  |
|    0x0f    |    启动点火     |
|    0x20    |    能否使用     |
|    0x21    |    减少次数     |

串口收到写寄存器的指令，首先对数据的帧头进行判断，然后再检查对应的功能码，再执行相关的函数，对于命令进行解析并执行。

```c
/*
	* @name   Protocol_Analysis
	* @brief  协议分析
	* @param  UART -> 串口指针
	* @retval None      
*/
static void Protocol_Analysis(UART_t* UART) 
{
	UART_t* const xdata COM = UART;
	

		//校验地址
		if(((*(COM->pucRec_Buffer+0)) == Modbus.Head1)&&((*(COM->pucRec_Buffer+1)) == Modbus.Head2)  )
		{
			//处理数据 根据功能码对寄存器进行读写操作
			
			switch ((*(COM->pucRec_Buffer+2)))
      {
      	case FunctionCode_Read_Register:
					Modbus_Read_Register(COM);
      		break;
      	case FunctionCode_Write_Register:
					Modbus_Wrtie_Register(COM);
      		break;
				case FunctionCode_Write_Imei:
					Modbus_Write_Imei(COM);
      		break;
				case FunctionCode_Boot_The_Device:
					Modbus_Boot_The_Device(COM);
      		break;
				case FunctionCode_Heat_Ignition:
					Modbus_Heat_Ignition(COM);
      		break;
			
      	default:
      		break;
      }
			
		}
	}
```

### 2.2 ZKSW串口屏幕

![image-20220824143054573](.\assets\image-20220824143054573.png)

使用zksw的显示屏幕确实大大降低的开发难度，用户只要专注于逻辑代码的实现。

屏幕实现的功能如下：

- 通过RS485\TTL与单片机进行通信
  - 下发写寄存器指令控制JDQ
  - 收到寄存器状态更新控件信息
- 显示设备编码 、温度信息、设备运行时间

#### UI设计

使用圆角矩形将设备的功能进行分割，添加艾灸按键防止误触操作220V的JDQ造成危险，中间部分为设备的二维码、运行时间，用户可以可到自己的使用时间，右侧为电机驱动部分，采用触摸按压反馈实现对JDQ的点动操作，这与选中时有所区别的。

#### 按压点动的实现

要实现这种效果，需要在UI初始化中注册触摸监听，然后重写触摸接听接口函数，官方给的例子中只有单个触摸接口，而我们需要6个按压触摸，所以在重写函数接口中需要对哪一个元件进行判断，根据控件的ID号加以判断，按下发送打开的指令，当松手时发送关闭的指令。

```c
namespace {  // 加个匿名作用域，防止多个源文件定义相同类名，运行时冲突
// 实现触摸监听接口
class TouchListener : public ZKBase::ITouchListener {
public:
    virtual void onTouchEvent(ZKBase *pBase, const MotionEvent &ev) {
    	 switch(pBase->getID()) {
    	        case  ID_MAIN_ButtonUp1:
    	        	switch (ev.mActionStatus) {
    	        	        case MotionEvent::E_ACTION_DOWN:
    	        	        	//mButtonUp1Ptr->setText("按下");
    	        	        	sendProtocol(CMDID_JDQ1,WRITE_REGISTER, &OPEN, 1);
    	        	            break;
    	        	        case MotionEvent::E_ACTION_UP:
    	        	        	//mButtonUp1Ptr->setText("抬起");
    	        	        	sendProtocol(CMDID_JDQ1,WRITE_REGISTER, &CLOSE, 1);
    	        	            break;
    	        	        default:
    	        	            break;
    	        	        }
    	        	break;
    	        case  ID_MAIN_ButtonDown1:
    	        		 switch (ev.mActionStatus) {
    	        		     	 case MotionEvent::E_ACTION_DOWN:
									//mButtonUp1Ptr->setText("按下");
									sendProtocol(CMDID_JDQ2,WRITE_REGISTER, &OPEN, 1);
									break;
								case MotionEvent::E_ACTION_UP:
									//mButtonUp1Ptr->setText("抬起");
									sendProtocol(CMDID_JDQ2,WRITE_REGISTER, &CLOSE, 1);
									break;
								default:
									break;
								}
						break;
    	        case  ID_MAIN_ButtonUp2:
					 switch (ev.mActionStatus) {
							 case MotionEvent::E_ACTION_DOWN:
								//mButtonUp1Ptr->setText("按下");
								sendProtocol(CMDID_JDQ3,WRITE_REGISTER, &OPEN, 1);
								break;
							case MotionEvent::E_ACTION_UP:
								//mButtonUp1Ptr->setText("抬起");
								sendProtocol(CMDID_JDQ3,WRITE_REGISTER, &CLOSE, 1);
								break;
							default:
								break;
							}
					 break;
			 case  ID_MAIN_ButtonDown2:
					 switch (ev.mActionStatus) {
							 case MotionEvent::E_ACTION_DOWN:
								//mButtonUp1Ptr->setText("按下");
								sendProtocol(CMDID_JDQ4,WRITE_REGISTER, &OPEN, 1);
								break;
							case MotionEvent::E_ACTION_UP:
								//mButtonUp1Ptr->setText("抬起");
								sendProtocol(CMDID_JDQ4,WRITE_REGISTER, &CLOSE, 1);
								break;
							default:
								break;
							}
					 break;
			 case  ID_MAIN_ButtonLeft:
					 switch (ev.mActionStatus) {
							 case MotionEvent::E_ACTION_DOWN:
								//mButtonUp1Ptr->setText("按下");
								sendProtocol(CMDID_JDQ5,WRITE_REGISTER, &OPEN, 1);
								break;
							case MotionEvent::E_ACTION_UP:
								//mButtonUp1Ptr->setText("抬起");
								sendProtocol(CMDID_JDQ5,WRITE_REGISTER, &CLOSE, 1);
								break;
							default:
								break;
							}
					 break;
			 case  ID_MAIN_ButtonRight:
					 switch (ev.mActionStatus) {
							 case MotionEvent::E_ACTION_DOWN:
								//mButtonUp1Ptr->setText("按下");
								sendProtocol(CMDID_JDQ6,WRITE_REGISTER, &OPEN, 1);
								break;
							case MotionEvent::E_ACTION_UP:
								//mButtonUp1Ptr->setText("抬起");
								sendProtocol(CMDID_JDQ6,WRITE_REGISTER, &CLOSE, 1);
								break;
							default:
								break;
							}
					 break;

    	        	default:
    	        		break;
    	 }
    }
};
}
```



### 更新与BUG日志

#### 8.26更新

改成了双层板结构，使用光耦驱动继电器，目前为低电平触发。

#### 8.29 更新

> 解决串口数据大量发送导致串口屏幕不能使用

**bug：**

经过两次测试为点火定时器结束后一直触发触发器，导致串口发送大量重复数据。

通过重新点击7、8 可以短暂解决此现象。

思路：查看定时器相关的设置

#### 8.30号更新

更新行程开关中断，通过两个外部的行程开关触发中断，

目前存在一个问题，只有外部中断0触发，外部中断1不触发。

思路：代码有问题，或者引脚又问题，重新焊接一套板子，测试一下。

#### 9.3更新与现场测试

完成屏幕设置温度阈值，将温度的阈值发送给单片机，单片机根据阈值关闭设备的风机

行程开关触发符合要求

#### 9.8更新

屏幕增加一键启动













