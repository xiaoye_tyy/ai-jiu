#pragma once
#include "uart/ProtocolSender.h"
/*
*此文件由GUI工具生成
*文件功能：用于处理用户的逻辑相应代码
*功能说明：
*========================onButtonClick_XXXX
当页面中的按键按下后系统会调用对应的函数，XXX代表GUI工具里面的[ID值]名称，
如Button1,当返回值为false的时候系统将不再处理这个按键，返回true的时候系统将会继续处理此按键。比如SYS_BACK.
*========================onSlideWindowItemClick_XXXX(int index) 
当页面中存在滑动窗口并且用户点击了滑动窗口的图标后系统会调用此函数,XXX代表GUI工具里面的[ID值]名称，
如slideWindow1;index 代表按下图标的偏移值
*========================onSeekBarChange_XXXX(int progress) 
当页面中存在滑动条并且用户改变了进度后系统会调用此函数,XXX代表GUI工具里面的[ID值]名称，
如SeekBar1;progress 代表当前的进度值
*========================ogetListItemCount_XXXX() 
当页面中存在滑动列表的时候，更新的时候系统会调用此接口获取列表的总数目,XXX代表GUI工具里面的[ID值]名称，
如List1;返回值为当前列表的总条数
*========================oobtainListItemData_XXXX(ZKListView::ZKListItem *pListItem, int index)
 当页面中存在滑动列表的时候，更新的时候系统会调用此接口获取列表当前条目下的内容信息,XXX代表GUI工具里面的[ID值]名称，
如List1;pListItem 是贴图中的单条目对象，index是列表总目的偏移量。具体见函数说明
*========================常用接口===============
*LOGD(...)  打印调试信息的接口
*mTextXXXPtr->setText("****") 在控件TextXXX上显示文字****
*mButton1Ptr->setSelected(true); 将控件mButton1设置为选中模式，图片会切换成选中图片，按钮文字会切换为选中后的颜色
*mSeekBarPtr->setProgress(12) 在控件mSeekBar上将进度调整到12
*mListView1Ptr->refreshListView() 让mListView1 重新刷新，当列表数据变化后调用
*mDashbroadView1Ptr->setTargetAngle(120) 在控件mDashbroadView1上指针显示角度调整到120度
*
* 在Eclipse编辑器中  使用 “alt + /”  快捷键可以打开智能提示
*/
bool UP_1_FLAG = false;
bool DOWN_1_FLAG = false;
bool UP_2_FLAG = false;
bool DOWN_2_FLAG = false;
bool RIGHT_FLAG = false;
bool LEFT_FLAG = false;

BYTE OPEN = 0x06;
BYTE CLOSE = 0x05;

namespace {  // 加个匿名作用域，防止多个源文件定义相同类名，运行时冲突
// 实现触摸监听接口
class TouchListener : public ZKBase::ITouchListener {
public:
    virtual void onTouchEvent(ZKBase *pBase, const MotionEvent &ev) {

    	 switch(pBase->getID()) {
    	        case  ID_MAIN_ButtonUp1:
    	        	switch (ev.mActionStatus) {
    	        	        case MotionEvent::E_ACTION_DOWN:
    	        	        	//mButtonUp1Ptr->setText("按下");
    	        	        	sendProtocol(CMDID_JDQ1,WRITE_REGISTER, &OPEN, 1);
    	        	            break;
    	        	        case MotionEvent::E_ACTION_UP:
    	        	        	//mButtonUp1Ptr->setText("抬起");
    	        	        	sendProtocol(CMDID_JDQ1,WRITE_REGISTER, &CLOSE, 1);
    	        	            break;
    	        	        default:
    	        	            break;
    	        	        }
    	        	break;
    	        case  ID_MAIN_ButtonDown1:
    	        		 switch (ev.mActionStatus) {
    	        		     	 case MotionEvent::E_ACTION_DOWN:
									//mButtonUp1Ptr->setText("按下");
									sendProtocol(CMDID_JDQ2,WRITE_REGISTER, &OPEN, 1);
									break;
								case MotionEvent::E_ACTION_UP:
									//mButtonUp1Ptr->setText("抬起");
									sendProtocol(CMDID_JDQ2,WRITE_REGISTER, &CLOSE, 1);
									break;
								default:
									break;
								}
						break;
    	        case  ID_MAIN_ButtonUp2:
					 switch (ev.mActionStatus) {
							 case MotionEvent::E_ACTION_DOWN:
								//mButtonUp1Ptr->setText("按下");
								sendProtocol(CMDID_JDQ3,WRITE_REGISTER, &OPEN, 1);
								break;
							case MotionEvent::E_ACTION_UP:
								//mButtonUp1Ptr->setText("抬起");
								sendProtocol(CMDID_JDQ3,WRITE_REGISTER, &CLOSE, 1);
								break;
							default:
								break;
							}
					 break;
			 case  ID_MAIN_ButtonDown2:
					 switch (ev.mActionStatus) {
							 case MotionEvent::E_ACTION_DOWN:
								//mButtonUp1Ptr->setText("按下");
								sendProtocol(CMDID_JDQ4,WRITE_REGISTER, &OPEN, 1);
								break;
							case MotionEvent::E_ACTION_UP:
								//mButtonUp1Ptr->setText("抬起");
								sendProtocol(CMDID_JDQ4,WRITE_REGISTER, &CLOSE, 1);
								break;
							default:
								break;
							}
					 break;
			 case  ID_MAIN_ButtonLeft:
					 switch (ev.mActionStatus) {
							 case MotionEvent::E_ACTION_DOWN:
								//mButtonUp1Ptr->setText("按下");
								sendProtocol(CMDID_JDQ5,WRITE_REGISTER, &OPEN, 1);
								break;
							case MotionEvent::E_ACTION_UP:
								//mButtonUp1Ptr->setText("抬起");
								sendProtocol(CMDID_JDQ5,WRITE_REGISTER, &CLOSE, 1);
								break;
							default:
								break;
							}
					 break;
			 case  ID_MAIN_ButtonRight:
					 switch (ev.mActionStatus) {
							 case MotionEvent::E_ACTION_DOWN:
								//mButtonUp1Ptr->setText("按下");
								sendProtocol(CMDID_JDQ6,WRITE_REGISTER, &OPEN, 1);
								break;
							case MotionEvent::E_ACTION_UP:
								//mButtonUp1Ptr->setText("抬起");
								sendProtocol(CMDID_JDQ6,WRITE_REGISTER, &CLOSE, 1);
								break;
							default:
								break;
							}
					 break;

    	        	default:
    	        		break;




    	 }


    }
};
}
static TouchListener touchListenerU1;
static TouchListener touchListenerU2;
static TouchListener touchListenerD1;
static TouchListener touchListenerD2;
static TouchListener touchListenerL;
static TouchListener touchListenerR;




/**
 * 注册定时器
 * 填充数组用于注册定时器
 * 注意：id不能重复
 */
static S_ACTIVITY_TIMEER REGISTER_ACTIVITY_TIMER_TAB[] = {
	//{0,  6000}, //定时器id=0, 时间间隔6秒
	//{1,  1000},

};

/**
 * 当界面构造时触发
 */
static void onUI_init(){
    //Tips :添加 UI初始化的显示代码到这里,如:mText1Ptr->setText("123");

	mButton_fanPtr->setTouchable(FALSE);
	mButton_firePtr->setTouchable(FALSE);
	mButton_hotPtr->setTouchable(FALSE);
	mButton_fengPtr->setTouchable(FALSE);

	//注册按键触摸监听
	mButtonUp1Ptr->setTouchListener(&touchListenerU1);
	mButtonUp2Ptr->setTouchListener(&touchListenerU2);
	mButtonDown1Ptr->setTouchListener(&touchListenerD1);
	mButtonDown2Ptr->setTouchListener(&touchListenerD2);
	mButtonLeftPtr->setTouchListener(&touchListenerL);
	mButtonRightPtr->setTouchListener(&touchListenerR);

}

/**
 * 当切换到该界面时触发
 */
static void onUI_intent(const Intent *intentPtr) {
    if (intentPtr != NULL) {
        //TODO
    }
}

/*
 * 当界面显示时触发
 */
static void onUI_show() {

}

/*
 * 当界面隐藏时触发
 */
static void onUI_hide() {

}

/*
 * 当界面完全退出时触发
 */
static void onUI_quit() {

	mButtonUp1Ptr->setTouchListener(NULL);
	mButtonDown1Ptr->setTouchListener(NULL);
	mButtonUp2Ptr->setTouchListener(NULL);
	mButtonDown2Ptr->setTouchListener(NULL);
	mButtonLeftPtr->setTouchListener(NULL);
	mButtonRightPtr->setTouchListener(NULL);

}

/**
 * 串口数据回调接口
 */
static void onProtocolDataUpdate(const SProtocolData &data) {
	if (data.state==0x03){
		mTextTempPtr->setText(data.temperature);
		mButtonUp1Ptr->setSelected(data.JDQ1);
		mButtonDown1Ptr->setSelected(data.JDQ2);
		mButtonUp2Ptr->setSelected(data.JDQ3);
		mButtonDown2Ptr->setSelected(data.JDQ4);
		mButtonLeftPtr->setSelected(data.JDQ5);
		mButtonRightPtr->setSelected(data.JDQ6);

		mButton_hotPtr->setSelected(data.JDQ8);
		mButton_fanPtr->setSelected(data.JDQ9);
		mButton_fengPtr->setSelected(data.JDQ10);
		mButton_firePtr->setSelected(data.JDQ11);
		}

}

/**
 * 定时器触发函数
 * 不建议在此函数中写耗时操作，否则将影响UI刷新
 * 参数： id
 *         当前所触发定时器的id，与注册时的id相同
 * 返回值: true
 *             继续运行当前定时器
 *         false
 *             停止运行当前定时器
 */
static bool onUI_Timer(int id){
	switch (id) {

		default:
			break;
	}
    return true;
}

/**
 * 有新的触摸事件时触发
 * 参数：ev
 *         新的触摸事件
 * 返回值：true
 *            表示该触摸事件在此被拦截，系统不再将此触摸事件传递到控件上
 *         false
 *            触摸事件将继续传递到控件上
 */
static bool onmainActivityTouchEvent(const MotionEvent &ev) {
    switch (ev.mActionStatus) {
		case MotionEvent::E_ACTION_DOWN://触摸按下
			//LOGD("时刻 = %ld 坐标  x = %d, y = %d", ev.mEventTime, ev.mX, ev.mY);
			break;
		case MotionEvent::E_ACTION_MOVE://触摸滑动
			break;
		case MotionEvent::E_ACTION_UP:  //触摸抬起
			break;
		default:
			break;
	}
	return false;
}
static bool onButtonClick_ButtonPower(ZKButton *pButton) {
    LOGD(" ButtonClick ButtonPower !!!\n");
    pButton->setSelected(!pButton->isSelected());
        if(pButton->isSelected()){
        	mButton_fanPtr->setTouchable(TRUE);
        	mButton_firePtr->setTouchable(TRUE);
        	mButton_hotPtr->setTouchable(TRUE);
    		mButton_fengPtr->setTouchable(TRUE);
        }
        else
        {
        	mButton_fanPtr->setTouchable(FALSE);
        	mButton_firePtr->setTouchable(FALSE);
        	mButton_hotPtr->setTouchable(FALSE);
        	mButton_fengPtr->setTouchable(FALSE);

        }
    return false;
}

static bool onButtonClick_Button_fan(ZKButton *pButton) {
    LOGD(" ButtonClick Button_fan !!!\n");
    pButton->setSelected(!pButton->isSelected());
	if(pButton->isSelected()){
		sendProtocol(CMDID_JDQ9, WRITE_REGISTER,&OPEN, 1);
	}
	else
	{
		sendProtocol(CMDID_JDQ9,WRITE_REGISTER, &CLOSE, 1);
	}
    return false;
}

static bool onButtonClick_Button_hot(ZKButton *pButton) {
    LOGD(" ButtonClick Button_hot !!!\n");
    pButton->setSelected(!pButton->isSelected());
	if(pButton->isSelected()){
		sendProtocol(CMDID_JDQ8,WRITE_REGISTER, &OPEN, 1);
	}
	else
	{
		sendProtocol(CMDID_JDQ8,WRITE_REGISTER, &CLOSE, 1);
	}
    return false;
}

static bool onButtonClick_Button_fire(ZKButton *pButton) {
    LOGD(" ButtonClick Button_fire !!!\n");
    pButton->setSelected(!pButton->isSelected());
	if(pButton->isSelected()){
		sendProtocol(CMDID_JDQ11,WRITE_REGISTER, &OPEN, 1);

	}
	else
	{
		sendProtocol(CMDID_JDQ11, WRITE_REGISTER,&CLOSE, 1);

	}
    return false;
}

static bool onButtonClick_Button_feng(ZKButton *pButton) {
    LOGD(" ButtonClick Button_feng !!!\n");
    pButton->setSelected(!pButton->isSelected());
	if(pButton->isSelected()){
		sendProtocol(CMDID_JDQ10,WRITE_REGISTER, &OPEN, 1);
	}
	else
	{
		sendProtocol(CMDID_JDQ10,WRITE_REGISTER, &CLOSE, 1);
	}
    return false;
}

static bool onButtonClick_ButtonDown1(ZKButton *pButton) {
    LOGD(" ButtonClick ButtonDown1 !!!\n");
    return false;
}

static bool onButtonClick_ButtonUp1(ZKButton *pButton) {
    LOGD(" ButtonClick ButtonUp1 !!!\n");
    return false;
}

static bool onButtonClick_ButtonDown2(ZKButton *pButton) {
    LOGD(" ButtonClick ButtonDown2 !!!\n");
    return false;
}

static bool onButtonClick_ButtonLeft(ZKButton *pButton) {
    LOGD(" ButtonClick ButtonLeft !!!\n");
    return false;
}

static bool onButtonClick_ButtonRight(ZKButton *pButton) {
    LOGD(" ButtonClick ButtonRight !!!\n");
    return false;
}

static bool onButtonClick_ButtonUp2(ZKButton *pButton) {
    LOGD(" ButtonClick ButtonUp2 !!!\n");
    return false;
}
static bool onButtonClick_Button_Setting(ZKButton *pButton) {
    LOGD(" ButtonClick Button_Setting !!!\n");
    EASYUICONTEXT->openActivity("windowActivity");
    return false;
}
