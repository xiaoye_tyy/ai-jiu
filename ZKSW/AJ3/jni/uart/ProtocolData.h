/*
 * ProtocolData.h
 *
 *  Created on: Sep 7, 2017
 *      Author: guoxs
 */

#ifndef _UART_PROTOCOL_DATA_H_
#define _UART_PROTOCOL_DATA_H_

#include <string>
#include "CommDef.h"

/******************** CmdID ***********************/

#define CMDID_POWER							0x00
#define CMDID_JDQ1							0x01
#define CMDID_JDQ2							0x02
#define CMDID_JDQ3							0x03
#define CMDID_JDQ4							0x04
#define CMDID_JDQ5							0x05
#define CMDID_JDQ6							0x06
#define CMDID_JDQ7							0x07
#define CMDID_JDQ8							0x08
#define CMDID_JDQ9							0x09
#define CMDID_JDQ10							0x0a
#define CMDID_JDQ11							0x0b
#define CMDID_JDQ12							0x0c
#define CMDID_ALL_INFO						0x0d
#define CMDID_TEMP_SENSOR					0x0e

#define CMDID_HIGH_TEMP						0x0f
#define CMDID_LOW_TEMP						0x10
#define CMDID_USE_NUM						0x11
/**************************************************/

//读寄存器
#define READ_REGISTER					 	0x03

#define UPDATE_IMEI							0x05
//写寄存器write
#define WRITE_REGISTER						0x06

#define UPDATE_DATE							0x07
/******************** 错误码 Error code ***********************/
#define ERROR_CODE_CMDID			1
/**************************************************/

typedef struct {
	BYTE power;
	BYTE JDQ1;
	BYTE JDQ2;
	BYTE JDQ3;
	BYTE JDQ4;
	BYTE JDQ5;
	BYTE JDQ6;
	BYTE JDQ7;
	BYTE JDQ8;
	BYTE JDQ9;
	BYTE JDQ10;
	BYTE JDQ11;
	BYTE JDQ12;
	BYTE temperature;
	BYTE state;
} SProtocolData;

#endif /* _UART_PROTOCOL_DATA_H_ */
